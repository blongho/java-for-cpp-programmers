/**
 * @file lab8 - FilterXMLFiles.java
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-01-24
 * @version 1.0
 */
package dt062g.lobe1602.assignment8;

import java.io.File;
import java.io.FilenameFilter;

/**
 * @author blongho
 *
 */
public class XMLFileNameFilter implements FilenameFilter {

	public static void main(String args[]) {
		//FilenameFilter filter = new FilterXMLFiles();

		File[] files = new File("xml").listFiles(new XMLFileNameFilter());

		for (File file : files) {
			System.out.print(file.getName());
			System.out.println("Size: " + file.length());

		}

	}

	/**
	 * Default constructor
	 */
	public XMLFileNameFilter() {

	}

	/**
	 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
	 */
	@Override
	public boolean accept(File dir, String name) {
		return name.toLowerCase().endsWith(".xml");
	}

}
