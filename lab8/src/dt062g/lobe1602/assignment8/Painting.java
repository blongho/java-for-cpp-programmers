/**
 * @file : Painting.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-09
 * @version : 1.0
 */
package dt062g.lobe1602.assignment8;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.xml.bind.JAXBException;

/**
 * JFrame class the displays drawings on the screen
 * 
 */
public class Painting extends JFrame implements ActionListener {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7471358965925057123L;

	private DrawingPanel drawingPanel; // area to display the drawings
	// sprivate Graphics g;
	private JMenu fileMenu, editMenu; // menues with submenues
	private JMenuBar menuBar; // main menu that holds all menuItems
	private JScrollPane scrollPane; // holds the drawingPanel
	private JComboBox<String> comboBox; // Displays menu for choosing shapes
	private JMenuItem newMenuItem, saveMenuItem, loadMenuItem, exitMenuItem,
			undoMenuItem, nameMenuItem, authorMenuItem, infoMenuItem;
	private JPanel statusBar; // panel that holds colorDisplay and coordinates
	private JLabel colorDisplay; // panel to show currently chosen paintColor
	private JLabel coordinates; // panel do display current mouse position
	private JPanel colorShapePanel; // panel that holds all available colors and
									// comboBox
	private String filename; // file from which drawing is to be loaded
	private String windowTitle; // title of this container (JFrame)
	private String author; // author of current drawing
	private String name; // name of current drawing
	private Drawing drawing; // a drawing to be displayed in the drawingPanel
	private Point point; // a point on the drawingPanel
	private String comboAction; // type of drawing chosen by user
	private Rectangle rectangle; // shape to be created
	private Circle circle; // circle to be created
	private Color paintColor; // color to be used for painting
	private Graphics graphics; // will be used to call draw(Graphics g)

	/**
	 * Starts the drawing panel with a no image
	 * 
	 * @throws HeadlessException
	 */
	public Painting() throws HeadlessException {
		super();
		name = author = filename = "";
		drawingPanel = new DrawingPanel();
		windowTitle = this.getClass().getSimpleName();

		paintColor = Color.GRAY; // starting painting color same as statusBar
		setLayout(new BorderLayout());

		setTitle(windowTitle);
		ImageIcon icon = new ImageIcon("img/shapes.png");
		setIconImage(icon.getImage());

		scrollPane = makeDisplayArea();

		menuBar = createMenuBar();

		setJMenuBar(menuBar);
		colorShapePanel = makeColorShapePanel();

		add(colorShapePanel, BorderLayout.PAGE_START);
		add(scrollPane, BorderLayout.CENTER);
		JPanel status = createStatusBar();
		add(status, BorderLayout.PAGE_END);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);

	}

	/**
	 * Make a display area and add events for tracking mouse position and for
	 * setting coordinates for shapes that the user wishes to draw
	 * 
	 * @return JScrollPane object with the text area
	 */
	private JScrollPane makeDisplayArea() {
		drawingPanel = new DrawingPanel(drawing);
		drawingPanel.setPreferredSize(new Dimension(600, 600));
		drawingPanel.setToolTipText("Images will be displayed here");
		drawingPanel.setBackground(Color.white);
		drawingPanel.setOpaque(true);
		drawingPanel.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				point = new Point(e.getX(), e.getY());
				updateCoordinates(point.toString());
			}
		});

		drawingPanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent press) {
				// System.err.println("mouse pressed at " + press.getPoint());
				Point p1 = new Point(press.getX(), press.getY());
				if (comboAction == "rectangle") {
					rectangle = new Rectangle(p1,
							StringColor.getHexColorString(paintColor));
					// System.out.println("First point of rectangle[" + p1 +
					// "]");
				} else if (comboAction == "circle") {
					circle = new Circle(p1,
							StringColor.getHexColorString(paintColor));
					// System.out.println("First point of circle[" + p1 + "]");
				}
			}

			@Override
			public void mouseReleased(MouseEvent release) {
				Point p2 = new Point(release.getX(), release.getY());
				if (comboAction == "rectangle") {
					rectangle.addPoint(p2);
					// rectangle.draw(graphics);
					// System.out.println("Second point of rectangle[" + p2 +
					// "]");
					// System.out.println(rectangle);

					if (drawing == null) {
						drawing = new Drawing();
						drawing.addShape(rectangle);
						drawingPanel.setDrawing(drawing);
					} else {
						try {
							Drawing d = new Drawing();
							d.addShape(rectangle);
							drawingPanel.addDrawing(d);
							drawingPanel.setDrawing(drawing);
						} catch (NullPointerException e) {
							System.err.println(e.getMessage());
						}
					}
				} else if (comboAction == "circle") {
					circle.addPoint(p2);
					// System.out.println("Second point of circle[" + p2 + "]");
					// System.out.println(circle);
					if (drawing == null) {
						drawing = new Drawing();
						drawing.addShape(circle);
						drawingPanel.setDrawing(drawing);
					} else {
						try {
							Drawing d = new Drawing();
							d.addShape(circle);
							drawingPanel.addDrawing(d);
							drawingPanel.setDrawing(drawing);
						} catch (NullPointerException e) {
							System.err.println(e.getMessage());
						}
					}
				}
			}

		});
		scrollPane = new JScrollPane(drawingPanel);

		return scrollPane;
	}

	/**
	 * Create the menu bar <br>
	 * Menu bar has two main menus [File|Edit]
	 * 
	 * @return menuBar with all its menu and sub-menu
	 */
	private JMenuBar createMenuBar() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.setToolTipText("Here you can manage files");

		newMenuItem = menuItem("New...", new ImageIcon("img/file.png"),
				KeyEvent.VK_N, true);
		newMenuItem.setToolTipText("Create a new file");

		saveMenuItem = menuItem("Save as...", new ImageIcon("img/save.gif"),
				KeyEvent.VK_S, true);
		saveMenuItem.setToolTipText("Save current drawing");

		loadMenuItem = menuItem("Load...", new ImageIcon("img/open.gif"),
				KeyEvent.VK_L, true);
		loadMenuItem.setToolTipText("Load image from file");

		infoMenuItem = menuItem("Info", new ImageIcon("img/info.jpg"),
				KeyEvent.VK_I, true);
		infoMenuItem.setToolTipText("Get image summary");

		exitMenuItem = menuItem("Exit", new ImageIcon("img/exit.png"),
				KeyEvent.VK_X, true);
		exitMenuItem.setToolTipText("Exits the program");

		// add file menu items
		fileMenu.add(newMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(loadMenuItem);
		fileMenu.add(infoMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);
		// System.out.println(fileMenu.getMenuComponentCount());

		menuBar.add(fileMenu);

		editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		editMenu.setToolTipText("Do some editing concerning a drawing");

		undoMenuItem = menuItem("Undo", new ImageIcon("img/undo.png"),
				KeyEvent.VK_U, true);
		undoMenuItem.setToolTipText("Remove the last drawn shape)");

		nameMenuItem = menuItem("Name...", new ImageIcon("img/name.png"),
				KeyEvent.VK_N, true);
		nameMenuItem.setToolTipText("Update the name of the drawing");

		authorMenuItem = menuItem("Author...", new ImageIcon("img/author.png"),
				KeyEvent.VK_A, true);
		authorMenuItem.setToolTipText("Update the author of the drawing");
		editMenu.add(undoMenuItem);
		editMenu.add(nameMenuItem);
		editMenu.add(authorMenuItem);

		menuBar.add(editMenu);

		menuBar.setVisible(true);

		return menuBar;

	}

	/**
	 * A small snippet to quickly make and add a menuItem to the main menu
	 * 
	 * @param title
	 *            The display text of the menu item
	 * @param image
	 *            The icon to display along-side the menu text
	 * @param hotkey
	 *            The keyboard short-cut to reach this menu item
	 * @return JMenuItem object
	 */
	private JMenuItem menuItem(String title, ImageIcon image, int hotkey,
			boolean action) {
		JMenuItem jMenuItem = new JMenuItem(title, image);
		jMenuItem.setHorizontalTextPosition(SwingConstants.RIGHT);
		jMenuItem.setMnemonic(hotkey);
		if (action) {
			jMenuItem.addActionListener(this);
		}
		return jMenuItem;

	}

	/**
	 * Build the color chooser and combo pane panel <br>
	 * Give each label object a mouseClick events and set the
	 * {@link paintColor}as well as the {@link colorDisplay} values <br>
	 * To this panel, the comboBox is also added alongside some action events
	 * that listens to get which shape the user wants to draw
	 * 
	 * @return JPanel object , the color and comboBox selection pane
	 */
	private JPanel makeColorShapePanel() {
		JPanel green, red, yellow, blue, black, cyan;
		green = new JPanel();
		red = new JPanel();
		yellow = new JPanel();
		blue = new JPanel();
		black = new JPanel();
		cyan = new JPanel();

		Vector<JPanel> lables = new Vector<JPanel>();
		lables.addElement(green);
		lables.addElement(red);
		lables.addElement(yellow);
		lables.addElement(blue);
		lables.addElement(black);
		lables.addElement(cyan);

		green.setBackground(Color.green);
		red.setBackground(Color.red);
		yellow.setBackground(Color.yellow);
		blue.setBackground(Color.blue);
		black.setBackground(Color.black);
		cyan.setBackground(Color.cyan);

		for (JPanel jLabel : lables) {
			jLabel.setOpaque(true);

			jLabel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getSource() == green) {
						colorDisplay.setBackground(Color.green);
						paintColor = Color.GREEN;
					} else if (e.getSource() == red) {
						colorDisplay.setBackground(Color.red);
						paintColor = Color.RED;
					} else if (e.getSource() == yellow) {
						colorDisplay.setBackground(Color.yellow);
						paintColor = Color.YELLOW;
					} else if (e.getSource() == blue) {
						colorDisplay.setBackground(Color.blue);
						paintColor = Color.BLUE;
					} else if (e.getSource() == black) {
						colorDisplay.setBackground(Color.black);
						paintColor = Color.BLACK;
					} else if (e.getSource() == cyan) {
						colorDisplay.setBackground(Color.cyan);
						paintColor = Color.CYAN;
					}
				}
			});
		}

		colorShapePanel = new JPanel();
		colorShapePanel.setLayout(new GridLayout(1, 7));
		colorShapePanel.setPreferredSize(new Dimension(100, 30));

		for (JPanel jLabel : lables) {
			colorShapePanel.add(jLabel);
		}

		// Create the combobox for selecting rectangle or circle
		comboBox = new JComboBox<>();
		comboBox.addItem("Rectangle");
		comboBox.addItem("Circle");
		comboBox.setEditable(false);
		comboBox.setSelectedItem("Circle");

		comboBox.setMinimumSize(new Dimension(30, 15));
		comboBox.setMaximumSize(comboBox.getMinimumSize());

		// set the value of comboAction based on the selected option
		comboBox.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedItem().equals("Rectangle")) {
					comboAction = "rectangle";
					// System.out.println("Rectangle drawing starts now");

				} else if (comboBox.getSelectedItem().equals("Circle")) {
					comboAction = "circle";
					// System.out.println("Circle drawing starts now");
				}
			}
		});

		colorShapePanel.add(comboBox);

		colorShapePanel.setVisible(true);
		return colorShapePanel;
	}

	/**
	 * - If the name menu is selected, the name of the drawing is changed to
	 * what the user enters <br>
	 * - If the author menuItem is selected, the author name of the current
	 * drawing is updated to the new one entered by the user <br>
	 * - If save item is selected, the drawing is saved to the file name
	 * specified <br>
	 * - If load menuItem is selected the following happens <br>
	 * i. If there is no drawing in the drawing panel, the loaded drawing
	 * becomes the current drawing <br >
	 * ii. If there is a drawing in the panel, the loaded drawing is added to it
	 * <br>
	 * - If the newMenuItem is selected the drawing area is cleared and the user
	 * is then free to assign new name for the drawing and author<br>
	 * - If the info menu is selected, the user gets summary of the current
	 * drawing in the drawing panel
	 * 
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == undoMenuItem) {
			if (drawing != null && drawing.getSize() > 0) {
				ArrayList<Shape> sh = drawing.getShapes();
				sh.remove(sh.size() - 1);
				drawing.setShapes(sh);
				drawingPanel.setDrawing(drawing);
				drawingPanel.repaint();
			} else {
				System.err.println("Cann't remove shape from an empty drawing");
			}

		} else if (e.getSource() == nameMenuItem) {
			name = setDrawingName();
			drawing.setName(name);
			updateWindowTitle();

		} else if (e.getSource() == authorMenuItem) {
			author = setDrawingAuthor();
			drawing.setAuthor(author);
			updateWindowTitle();

		} else if (e.getSource() == saveMenuItem) {
			filename = (String) JOptionPane.showInputDialog(null,
					"Save drawing to...", "File name",
					JOptionPane.QUESTION_MESSAGE, new ImageIcon("img/file.png"),
					null, getFileName(drawing));
			try {
				if (drawing != null) {
					FileHandler.saveToXML(drawing, filename);
					System.out.println("File saved");
				} else {
					System.err.println("Impossible saving an empty drawing.");
				}
			} catch (JAXBException | IOException e1) {
				// TODO Auto-generated catch block
				System.err.println(e1.getStackTrace());
			}

		} else if (e.getSource() == loadMenuItem) {
			filename = JOptionPane.showInputDialog(null, "Load drawing from...",
					"File to upload", JOptionPane.QUESTION_MESSAGE);

			if (filename != null) {
				try {
					if (drawing == null) {
						drawing = FileHandler.loadFromXML(filename);
						drawingPanel.setDrawing(drawing);
						author = drawing.getAuthor();
						name = drawing.getName();
						updateWindowTitle();
					} else {
						Drawing d2 = FileHandler.loadFromXML(filename);
						drawingPanel.addDrawing(d2);
						// drawingPanel.setDrawing(drawing);
						author = drawing.getAuthor();
						name = drawing.getName();
						updateWindowTitle();
					}
				} catch (JAXBException | IOException e1) {
					// TODO Auto-generated catch block
					System.err.println(e1.getMessage());
				}
			} else {
				System.err.println("No file name specified");
			}

		} else if (e.getSource() == infoMenuItem) {
			if (drawing != null) {
				JOptionPane.showMessageDialog(null, drawing.info(), "Info",
						JOptionPane.INFORMATION_MESSAGE);
			} else {
				JOptionPane.showMessageDialog(null, "No loaded image!", "Info",
						JOptionPane.INFORMATION_MESSAGE);
				System.err.println("No image specified");
			}

		} else if (e.getSource() == newMenuItem) {
			if (drawing != null) {
				drawing.clear();
			}
			name = setDrawingName();
			updateWindowTitle();
			author = setDrawingAuthor();
			updateWindowTitle();
			drawing = new Drawing(name, author);
			drawingPanel.setDrawing(drawing);

		} else if (e.getSource() == exitMenuItem) {
			System.exit(0);
		}
	}

	/**
	 * Create the status bar <br>
	 * This holds the pane that displays current mouse position as well as the
	 * current {@link paitColor}
	 * 
	 * @return the frame's status bar
	 */
	private JPanel createStatusBar() {
		BorderLayout borderLayout = new BorderLayout();

		statusBar = new JPanel(borderLayout);
		coordinates = new JLabel("Coordinates: 0.0", SwingConstants.LEFT);
		statusBar.add(coordinates, BorderLayout.WEST);

		JPanel colorPane = new JPanel();
		JLabel selectText = new JLabel("Selected color : ");
		colorPane.add(selectText, BorderLayout.WEST);

		colorDisplay = new JLabel();
		colorDisplay.setOpaque(true);
		colorDisplay.setBackground(Color.gray);
		colorDisplay.setPreferredSize(new Dimension(20, 20));
		colorDisplay.setBorder(BorderFactory.createRaisedBevelBorder());
		colorDisplay.setToolTipText("Current color for drawing");
		colorPane.add(colorDisplay, BorderLayout.EAST);
		colorPane.setBackground(Color.GRAY);
		statusBar.setBackground(Color.gray);
		statusBar.add(colorPane, BorderLayout.EAST);
		statusBar.setOpaque(true);

		return statusBar;
	}

	/**
	 * Update the value of coordinates when user clicks a point on the screen
	 * 
	 * @param text
	 *            A point object returned to string
	 */
	private void updateCoordinates(String text) {
		String string[] = coordinates.getText().split(" ");
		coordinates.setText(string[0] + " " + text);
	}

	/**
	 * Dynamically update the window title as the user enters values for the
	 * painting (name) and author
	 */
	private void updateWindowTitle() {
		String tmp = "";
		if (!author.isEmpty() || !name.isEmpty()) {
			setTitle(windowTitle + " - " + tmp);
		}
		if (!author.isEmpty() && !name.isEmpty()) {
			tmp = name + " by " + author;
		} else if (!author.isEmpty() && name.isEmpty()) {
			tmp = author;
		} else if (author.isEmpty() && !name.isEmpty()) {
			tmp = name;
		}
		setTitle(windowTitle + " - " + tmp);
		if (!author.isEmpty() || !name.isEmpty()) {
			setTitle(windowTitle + " - " + tmp);
		}
	}

	/**
	 * Let the user enter the new name for the drawing or edit it
	 */
	private String setDrawingName() {
		name = JOptionPane.showInputDialog(null,
				"Enter the name of the drawing", "Name of drawing",
				JOptionPane.QUESTION_MESSAGE);
		return name;

	}

	/**
	 * Let the user enter a new author name or edit the current one
	 */
	private String setDrawingAuthor() {
		author = JOptionPane.showInputDialog(null, "Enter the author name",
				"Author's name", JOptionPane.QUESTION_MESSAGE);
		return author;
	}

	/**
	 * Derive the file name for a drawing
	 * 
	 * @param drawing
	 *            A drawing to save or read from file
	 * @return a valid file name that ends with .xml
	 */
	public String getFileName(Drawing drawing) {
		String sample = "";
		if (author.isEmpty() && name.isEmpty()) {
			sample = ".xml";
		} else if (author.isEmpty() && !name.isEmpty()) {
			sample = name + ".xml";
		} else if (!author.isEmpty() && !name.isEmpty()) {
			sample = name + " by " + author + ".xml";
		}
		return sample;
	}

}