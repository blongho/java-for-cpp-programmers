/**
 * @file : DrawingPanel.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-07
 * @version : 1.0
 */
package dt062g.lobe1602.assignment8;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

/**
 * Class that defines the area for painting a drawing
 */
public class DrawingPanel extends JPanel {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 3393672211744307463L;
	private Drawing drawing;

	/**
	 * An empty constructor that creates a new empty drawing object
	 */
	public DrawingPanel() {
		setDrawing(new Drawing());
	}

	/**
	 * A drawingPanel constructor that takes a drawing object
	 * 
	 * @param drawing
	 */
	public DrawingPanel(Drawing drawing) {
		if (drawing != null) {
			this.setDrawing(drawing);
			repaint();
		}
	}

	/**
	 * Add shapes from a drawing into the current drawing <br />
	 * NB: Name of the current drawing remains the same
	 * 
	 * @param d
	 *            The drawing whose shapes are to be added to the current
	 *            drawing
	 */
	public void addDrawing(Drawing d) {
		String currentName = drawing.getName();
		String currentAuthor = drawing.getAuthor();

		for (Shape shape : d.getShapes()) {
			this.drawing.addShape(shape);
		}
		// this.setDrawing(drawing);
		drawing.setAuthor(currentAuthor);
		drawing.setName(currentName);

		repaint();
	}

	/**
	 * Set a new drawing to replace the current drawing. <br/>
	 * After the setting, the drawing area is updated
	 * 
	 * @param drawing
	 *            the drawing to set
	 */
	public void setDrawing(Drawing drawing) {
		this.drawing = drawing;
		repaint();
	}

	/**
	 * Get the current drawing
	 * 
	 * @return the drawing
	 */
	public Drawing getDrawing() {
		return drawing;
	}

	/**
	 * Drawing the current drawing by calling the draw method
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		if (drawing != null) {
			super.paintComponent(g);
			for (Shape shape : drawing.getShapes()) {
				shape.draw(g);
			}
		} else {
			setBackground(Color.WHITE);
			// System.err.println("Drawing area has no shapes!");
		}

	}

}
