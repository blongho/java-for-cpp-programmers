/**
 * @file lab8 - Client.java
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-01-24
 * @version 1.0
 */
package dt062g.lobe1602.assignment8.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.URL;

import dt062g.lobe1602.assignment8.XMLFileNameFilter;

/**
 * @author blongho
 *
 */
public class Client {
	public static String DEFAULT_ADDRESS = "localhost";
	public static int DEFAULT_PORT = 10000;
	private static String SERVER_DIRECTORY = "xml";
	private static Socket socket = null;
	private static String serverAddress;
	private static int port;
	private static DataOutputStream out = null;
	private static DataInputStream in = null;

	/**
	 * 
	 */
	public Client() {
		Client.port = DEFAULT_PORT;
		Client.serverAddress = DEFAULT_ADDRESS;
	}

	/**
	 * Create a new client with different address and port number
	 * 
	 * @param serverAddress
	 *            the server address
	 * @param port
	 *            the client's port number
	 */
	public Client(final String serverAddress, final int port) {
		Client.serverAddress = serverAddress;
		Client.port = port;

	}

	/**
	 * Try to connect to the server and <br>
	 * - make the connection <br>
	 * - Create communication streams <br>
	 * pre: If there is already a connection, no connection is created else create
	 * one <br>
	 * post: Return true if connection and streams are created else false
	 */
	private static boolean connect() {
		if (socket == null) {
			try {
				socket = new Socket(serverAddress, port);
				out = new DataOutputStream(socket.getOutputStream());
				in = new DataInputStream(socket.getInputStream());
				return true;
			} catch (IOException e) {
				System.err.println("Error connecting with server " + e.getMessage());
				return false;
			}
		}
		return false;
	}

	/**
	 * Disconnect the socket from the server and set both socket and streams to null
	 */
	private static void disconnect() {
		try {
			if (socket != null) {
				socket.close();
				socket = null;
			}

			if (in != null) {
				in.close();
				in = null;
			}
			if (out != null) {
				out.flush();
				out.close();
				out = null;
			}

		} catch (IOException e) {
			System.err.println("Error closing socket and streams " + e.getMessage());
		}
	}

	/**
	 * Get the server directory
	 * 
	 * @return return the server directory
	 */
	private static File getServerDirectory() {
		try {
			File directory = new File(SERVER_DIRECTORY);

			if (directory.isDirectory()) {
				return directory;

			} else {
				System.err.println("Error retrieving server directory");
				return null;
			}
		} catch (Exception e) {
			System.err.println("The directory '" + SERVER_DIRECTORY + "' could not be read");
			return null;
		}
	}

	/**
	 * Create a client directory
	 * 
	 * @return client directory {clientFiles}
	 */
	private static File getClientDirectory() {
		File clientDirectory = new File("clientFiles");
		if (!clientDirectory.exists()) {
			try {
				clientDirectory.mkdirs();
			} catch (SecurityException e) {
				System.err.println("Error creating client directory " + e.getMessage());
				// return null;
			}
		}
		return clientDirectory;
	}

	/**
	 * Get all the files names of all .xml files in the server
	 * 
	 * @return null if something went wrong, or array filled with file names or
	 *         array of length 0 if there are no .xml files in the directory
	 */
	static String[] getFilenamesFromServer() {
		String filesNames[] = new String[0];
		if (connect()) {
			try {
				File directory = getServerDirectory();

				if (directory != null) {
					File files[] = directory.listFiles(new XMLFileNameFilter());

					if (files.length > 0) {
						filesNames = new String[files.length];
						for (int i = 0; i < files.length; i++) {
							if (files[i].isFile()) {
								filesNames[i] = files[i].getName();
							}
						}
					} else {
						System.err.println("'" + directory.getName() + "' has no .xml files");
						disconnect();
						return null;
					}
				}

			} catch (Exception e) {
				System.err.println("Error reading from server directory " + e.getMessage());
				disconnect();
				return null;
			}
		}
		disconnect();
		return filesNames;
	}

	/**
	 * Get a file from the server and save it in the client's hard drive.
	 * 
	 * @param filename
	 *            the name of the file to retrieve
	 * @return the full path of the file in the client's hard drive if everything
	 *         went well. Otherwise and empty string is returned
	 */
	public static String getFileFromServer(final String filename) {
		if (connect()) {
			try {
				File serverDirectory = getServerDirectory();
				disconnect();
				return transferFile(filename, serverDirectory, getClientDirectory(), null);

			} catch (Exception e) {
				System.err.println(e.getMessage());
				disconnect();
				return null;
			}
		}
		return null;
	}

	/**
	 * Transfer files from one directory to another
	 * 
	 * @param searchFile
	 *            The file to be transfered
	 * @param fromDir
	 *            The directory containing the original file
	 * @param toDir
	 *            The directory to hold the new file
	 * @param newFile
	 *            The name of the new file (if the name is null, the new file
	 *            assumes the name of the original file)
	 * @return a string containing the full path of the containing file or an empty
	 *         string if the file is not created or null if an error occurred during
	 *         file creation.
	 */
	private static String transferFile(final String searchFile, final File fromDir, final File toDir,
					final String newFile) {
		FileOutputStream fop = null;
		FileInputStream fin = null;
		String savePath = "";
		boolean fileFound = false;
		if (fromDir.exists()) {
			for (File file : fromDir.listFiles()) {
				if (file.getName().equalsIgnoreCase(searchFile)) {
					fileFound = true;
					String filename = newFile == null ? file.getName() : newFile;
					savePath = toDir + "/" + filename;

					byte[] content = new byte[(int) file.length()];

					try {
						fop = new FileOutputStream(savePath);
						fin = new FileInputStream(file);
						fin.read(content);
						fop.write(content);

						fop.flush();
						fop.close();
						fin.close();
						savePath = new File(savePath).getAbsolutePath();
					} catch (IOException e) {
						System.err.println("File transfer error : " + e.getMessage());
						return null;
					}
				}
			}
			if (!fileFound) {
				System.err.println(searchFile + " not part of the directory " + fromDir.getName());
				return null;
			}
		} else {
			System.err.println(fromDir.getName() + " does not exist");
			return null;
		}
		return savePath;
	}

	/**
	 * Save a local file to the server
	 * 
	 * @param fromFileName
	 *            the file from the client to be saved in the server
	 * @param toFileName
	 *            the name the file should take in the server
	 * @return true if retrieve/send operations goes well, otherwise false
	 */
	public static boolean saveAsFileToServer(final String fromFileName, final String toFileName) {

		File serverDir = getServerDirectory();
		String path = "";
		if (serverDir != null) {
			if (connect()) {
				try {
					path = transferFile(fromFileName, getClientDirectory(), serverDir, toFileName);
					if (!path.isEmpty() && path != null) {
						disconnect();
						return true;
					}
				} catch (Exception e) {
					System.err.println("Error transferring files: " + e.getMessage());
					disconnect();
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Transfer a file from the client to the server. The server file has the same
	 * name as the client file
	 * 
	 * @param fromFileName
	 *            The file to search for from the client
	 * @return True if everything went well, other false;
	 */
	public static boolean saveFileToServer(final String fromFileName) {
		File serverDir = getServerDirectory();
		String path = "";
		if (serverDir != null) {
			if (connect()) {
				try {
					path = transferFile(fromFileName, getClientDirectory(), serverDir, null);
					if (!path.isEmpty() && path != null) {
						disconnect();
						return true;
					}
				} catch (Exception e) {
					System.err.println("Error transferring files: " + e.getMessage());
					disconnect();
					return false;
				}
			}
		}
		return false;
	}

	/**
	 * Method that wraps all the possible interactions with the server <br>
	 * 
	 * @param command
	 *            The command that the client wants the server to perform
	 */
	public static void doCommand(final String command) {
		new Client();
		switch (command) {
		case "list":
			if (connect()) {
				System.out.println("\n");
				System.out.println("Client: Server, i need list of all .xml files");
				try {
					out.writeUTF("list");
					out.flush();
					Thread.sleep(1000);
					System.out.println(in.readUTF());
					Thread.sleep(1000);
					System.out.print(in.readUTF());
					Thread.sleep(1000);
					System.out.println("Clint: Thanks");
					out.writeUTF("Thanks");
					Thread.sleep(1000);
					System.out.println(in.readUTF());

				} catch (Exception e) {
					System.err.println("Error communicating with server from Client.java " + e.getMessage());
					disconnect();
				}
				disconnect();
			}
			break;
		case "load":
			if (connect()) {
				try {
					System.out.println("\n\n");
					System.out.println("Client: Server, i want a file");
					out.writeUTF("load");
					out.flush();
					System.out.println(in.readUTF());
					out.writeUTF("Rectangles by Robert.xml");
					out.flush();
					System.out.println("Client: Rectangles by Robert, please");
					System.out.println(in.readUTF());

					if (in.readUTF().equals("Server : File sent")) {
						out.writeUTF("Client : File received");
						out.flush();
						System.out.print("Client: File received");
					}
				} catch (Exception e) {
					System.err.println("Error communicating with server from Client.java " + e.getMessage());
					disconnect();
				}
				disconnect();
			}
			break;
		case "save":
			if (connect()) {
				try {
					out.writeUTF("save");
					out.flush();
					System.out.println("\n\n");
					System.out.println("Client: Transfering berns.xml to server");
					out.writeUTF("berns.xml");
					out.flush();

					System.out.println(in.readUTF());
					System.out.println(in.readUTF());

				} catch (Exception e) {
					System.err.println("Error communicating with server from Client.java " + e.getMessage());
					disconnect();
				}
				disconnect();
			}
			break;
		}

	}

	public static void main(final String[] args) {
		
		Client.doCommand("list");
		//Client.doCommand("save");
		//Client.doCommand("load");

		
	}
}
