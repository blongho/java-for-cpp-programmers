/**
 * @file lab8 - ClientHandler.java
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-01-24
 * @version 1.0
 */
package dt062g.lobe1602.assignment8.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;

/**
 * @author blongho
 *
 */
public class ClientHandler extends Thread {
	private Socket socket;
	DataInputStream input = null;
	DataOutputStream output = null;
	Client client;
	String clientAddress;

	/**
	 * @param clientSocket
	 */
	public ClientHandler(final Socket socket) {
		this.socket = socket;

	}

	@Override
	public void run() {

		try {
			while (true) {
				input = new DataInputStream(socket.getInputStream());
				output = new DataOutputStream(socket.getOutputStream());

				String command = input.readUTF();
				clientAddress = socket.getInetAddress().getHostAddress() + ":" + socket.getPort();
				System.out.printf("Server: Command '%s' recieved from %s.%n", command, clientAddress);

				switch (command) {
				case "list":
					System.out.println("\nServer: Sending list of .xml files to " + clientAddress + ".");
					output.writeUTF("Server: Here is your list...");
					client = new Client();
					String[] filesnames = Client.getFilenamesFromServer();
					StringBuffer buffer = new StringBuffer("");
					int i = 0;
					for (String name : filesnames) {
						buffer.append("\t" + (++i) + ". " + name + "\n");
					}
					output.writeUTF(buffer.toString());
					output.flush();
					if (input.readUTF().equalsIgnoreCase("Thanks")) {
						output.writeUTF("Server : You are welcome dear. Bye!");
					}
					closeStreams();
					break;

				case "load":
					Thread.sleep(1000);
					output.writeUTF("Server : Which file do you want me to load?");
					output.flush();
					String fileName = input.readUTF();
					client = new Client();
					String fil = Client.getFileFromServer(fileName);
					if (fil != null) {
						File file = new File(fil);
						output.writeUTF("Server : Size of '" + fileName + "' is " + file.length() + " bytes");
						output.flush();
						output.writeUTF("Server : File sent");
						output.flush();
						System.out.println("Server: Size '" + fileName +"' is " + file.length() + " bytes");
						System.out.println("Seding file to " + clientAddress);
						System.out.println("File sent to " + clientAddress);
					}
					closeStreams();
					break;

				case "save":
					String filename = input.readUTF();
					output.writeUTF(new String(String.format("Receiving '%s' from %s", filename, clientAddress)));
					output.flush();
					System.out.printf("Server: Receiving '%s' from %s%n", filename, clientAddress);
					client = new Client();
					if (Client.saveAsFileToServer(filename, filename.replace(".xml", " (copy).xml"))) {
						output.writeUTF(new String(String.format("Server: %s received from %s",
										filename.replaceAll(".xml", " (copy).xml"), clientAddress)));
						output.flush();
					}

					closeStreams();
					break;

				case "exit":
					System.out.println("Client %s requested to close connection." + clientAddress);
					closeStreams();
					break;
				}
			}
		} catch (Exception e) {
			System.err.println("Connection exception in clientHandler " + e.getMessage());
			closeStreams();
		}
	}
	private void closeStreams() {
		try {
			if (input != null) {
				input.close();
			}
			if (output != null) {
				output.flush();
				output.close();
			}
			if (socket != null) {
				socket.close();
			}
		} catch (IOException e2) {
			System.err.println("Error closing streams " + e2.getMessage());
		}
	}
}
	// if (command.equals("list")) {
	// System.out.println("\nServer: Sending list of .xml files to " + clientAddress
	// + ".");
	// output.writeUTF("Server: Here is your list...");
	// client = new Client();
	// String[] filesnames = Client.getFilenamesFromServer();
	// StringBuffer buffer = new StringBuffer("");
	// int i = 0;
	// for (String name : filesnames) {
	// buffer.append("\t" + (++i) + ". " + name + "\n");
	// }
	// output.writeUTF(buffer.toString());
	// output.flush();
	// if (input.readUTF().equalsIgnoreCase("Thanks")) {
	// output.writeUTF("Server : You are welcome dear. Bye!");
	// }
	// closeStreams();
	// } else if (command.equals("load")) {
	// Thread.sleep(2000);
	// output.writeUTF("Server : Which file do you want me to load?");
	// output.flush();
	// String fileName = input.readUTF();
	// client = new Client();
	// String fil = Client.getFileFromServer(fileName);
	// if (fil != null) {
	// File file = new File(fil);
	// output.writeUTF("Server : Size of '" + fileName + "' is " + file.length() + "
	// bytes");
	// output.flush();
	// output.writeUTF("Server : File sent");
	// output.flush();
	// System.out.printf("Server: Size %s is %f bytes%n", fileName, file.length());
	// System.out.println("Seding file to " + clientAddress);
	// System.out.println("File sent to " + clientAddress);
	//
	// closeStreams();
	// } else {
	// System.out.println("No file with the name '" + fileName + "' found!");
	// output.writeUTF("bad");
	// output.flush();
	// output.writeUTF("No file with the given name found");
	// output.flush();
	// closeStreams();
	// }
	// } else if (command.equals("save")) {
	// String filename = input.readUTF();
	// output.writeUTF(new String(String.format("Receiving '%s' from %s", filename,
	// clientAddress)));
	// output.flush();
	// System.out.printf("Server: Receiving '%s' from %s%n", filename,
	// clientAddress);
	// client = new Client();
	// if (Client.saveAsFileToServer(filename, filename.replace(".xml", "
	// (copy).xml"))) {
	// output.writeUTF(new String(String.format("Server: %s received from %s",
	// filename.replaceAll(".xml", " (copy).xml"), clientAddress)));
	// output.flush();
	// }
	//
	// closeStreams();
	// } else if (command.equals("exit")) {
	// System.out.println("Client %s requested to close connection." +
	// clientAddress);
	// closeStreams();
	// break;
	//
	// }
	// }

	

