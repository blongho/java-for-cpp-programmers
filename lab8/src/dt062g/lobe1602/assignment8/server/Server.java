/**
 * @file lab8 - Server.java
 * @author Bernard Che Longho (lobe1602)
 * @since 2018-01-24
 * @version 1.0
 */
package dt062g.lobe1602.assignment8.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import dt062g.lobe1602.assignment8.client.ClientHandler;

/**
 * @author blongho
 *
 */
public class Server {
	private static int DEFAULT_PORT_NUMBER = 10000;

	/**
	 * @param port
	 *            The port that server listens to
	 */
	public Server(final int port) {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			System.out.println("Server started at port " + port);
		} catch (final IOException e1) {
			System.err.println("Server socket creation error: " + e1.getMessage());
			return;
		}
		// connect with clients
		while (true) {
			Socket clientSocket = null;
			try {
				clientSocket = serverSocket.accept();
				String clientAddress = clientSocket.getInetAddress().getHostAddress() + ":" + clientSocket.getPort();
				System.out.println("\nNew client connected from " + clientAddress);
				
				if(!clientSocket.isClosed()) {
					System.out.printf("Client from %s has disconnected.%n", clientAddress);
				}
				new ClientHandler(clientSocket).start();
				} catch (final IOException e) {
				System.err.println("Error establishing connection with client " + e.getMessage());
			}
		}

	}

	/**
	 * This program expects the user to enter the port number like <br>
	 * <code>java Server port</code> If no argument is given, the default port
	 * number (10000) is used.
	 * 
	 * @param The
	 *            port number for the server to listen from (int)
	 * 
	 */
	public static void main(final String[] args) {
		
		Server server = null;
		if (args.length > 0) {
			try {
				final int portnr = Integer.parseInt(args[0]);
				server = new Server(portnr);
			} catch (final NumberFormatException e) {
				System.err.println("Error converting '" + args[0] + "' to a valid integer");
				return;
			}
		} else {
			server = new Server(DEFAULT_PORT_NUMBER);
		}
	}

}
