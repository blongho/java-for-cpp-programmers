/**
 * <h1>Circle</h1>
 * This class contains the methods for manipulating a circle. 
 * It inherits from the <code>Shape</code> class.
 * @author	Bernard Che Longho (lobe1602)
 * @version	1.0
 * @since	2017-11-15
 */
package dt062g.lobe1602.assignment2;

import java.awt.Graphics;


/**
 * <code>Circle</code> 
 * This class inherits from Shape
 */
public class Circle extends Shape {
	private final double pi = 3.14; /** Approximate value of pi */
	/**
	 * Default constructor1 of <code>Circle</code>
	 * Takes its coordinates from x and y coordinates
	 * @param x The x-coordinate
	 * @param y	The y-coordinate
	 * @param col The fill color
	 */
	public Circle(double x, double y, String col) {
		super(x, y, col);
		}

	/**
	 * Default constructor2 of <code>Circle</code> 
	 * Takes its coordinates from the x- and y-coordinates from a point object
	 * @param p	Point object with the coordinates that determine the location of
	 * the circle
	 * @param col	The fill color of the circle
	 */
	public Circle(Point p, String col) {
		super(p, col);
		}
	
	/**
	 * Calculate and return the radius of a circle.
	 * If only 1 point is given, -1 is returned. 
	 * If two points are given, then x2 - x1 will give us the circle
	 * @return radius The radius of the circle
	 */
	public double getRadius() {
		if(this.points[1] != null) {
			double x1 = this.points[0].getXcord();
			double x2 = this.points[1].getXcord();
			double y1 = this.points[0].getYcord();
			double y2 = this.points[1].getYcord();
			
			return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
		}
		return -1;
	}
	
	/**
	 * Calculate and return the circumference of the circle
	 * If radius is defined, use 2*pi*radius to get the circumference.
	 * Otherwise, return -1.
	 * @see dt062g.lobe1602.assignment2.Shape#getCircumference()
	 */
	@Override
	public double getCircumference() {
		if(this.getRadius() >= 0) {
			return (2 * pi * this.getRadius());
		}
		return -1;
	}

	/**
	 * Calculate and return the area of the circle.
	 * If radius is defined, use pi*radius*radius
	 * Otherwise, return -1
	 * @see dt062g.lobe1602.assignment2.Shape#getArea()
	 */
	@Override
	public double getArea() {
		if(this.getRadius() >= 0) {
			return pi * this.getRadius() * this.getRadius();
		}
		return -1;
	}


	/**
	 * @see dt062g.lobe1602.assignment2.Shape#draw()
	 */
	@Override
	public void draw() {
		System.out.println("Drawing a " + this.toString());
		}

	/**
	 * @see dt062g.lobe1602.assignment2.Shape#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {}
		
	/**
	 * Overload the toString method to print a circle object on the screen
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(getRadius() >= 0) {
			return "Circle[start=" + this.points[0] + "; end=" + this.points[1] +
					"; radius=" + this.getRadius() + "; color= " + this.getColor() + "]";
		}
		else {
			return "Circle[start=" + this.points[0] + "; end=N/A; radius=N/A;"
					+ " color= " + this.getColor() + "]";
		}
	}
	
}
