/**
 * <h1>Point</h1>
 * A class to represent x and y coordinates of any object(shape)
 * @author	Bernard Che Longho (lobe1602)
 * @version	1.0
 * @since	2017-11-15
 */
package dt062g.lobe1602.assignment2;

/**
 * <code>Point</code> class declared public as it is designed to be used by
 * other classes
 */
public class Point {
	private double xcord; /** The x-coordinate */
	private double ycord; /** The y-coordinate */
	
	// Constructors
	/**
	 * Default constructor of <code>Point</code> with default values for
	 * x and y coordinates set to 0 (zero)
	 */
	public Point() {
		xcord = ycord = 0;
	}
	
	/**
	 * Default construct for <code>Point</code> with values set for the 
	 * x and y coordinates
	 * @param x The value of the x-coordinate
	 * @param y The value of the y-coordinate
	 */
	public Point(double xcord, double ycord) {
		this.xcord = xcord;
		this.ycord = ycord;
	}
	
	
	// Methods
	/**
	 * Get the value of the x-coordinate
	 * @return the xcord
	 */
	public double getXcord() {
		return xcord;
	}
	
	/**
	 * Set a new value for the x-coordinate
	 * @param xcord the xcord to set
	 */
	public void setXcord(double xcord) {
		this.xcord = xcord;
	}
	
	/**
	 * Return the y coordinate
	 * @return the ycord
	 */
	public double getYcord() {
		return ycord;
	}
	
	/**
	 * Set a new value for the y coordinate
	 * @param ycord the ycord to set
	 */
	public void setYcord(double ycord) {
		this.ycord = ycord;
	}
	
	/** 
	 * Overload the toString method to allow a representation of a 
	 * <code>Point</code> object as a string.
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.xcord + ", " + this.ycord;
	}
}