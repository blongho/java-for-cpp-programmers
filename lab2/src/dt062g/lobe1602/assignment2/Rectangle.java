/**
 * <h1>Rectangle</h1>
 * Rectangle.java
 * @author	Bernard Che Longho (lobe1602)
 * @version	1.0
 * @since	2017-11-15
 * 
*/
package dt062g.lobe1602.assignment2;

import java.awt.Graphics;

/**
 * Rectangle class inherits from <code>Shape</code>
 * This class has no instance variables. It gets its coordinates from 
 * <code>Point</code> 
 */
public class Rectangle extends Shape {

	/**
	 * Default contructor1
	 * @param x The x-coordinate
	 * @param y	The y-coordinate
	 * @param col The fill color
	 */
	public Rectangle(double x, double y, String col) {
		super(x, y, col);
	}

	/**
	 * Default constructor2
	 * @param p	<code>Point</code> in the x-y plane
	 * @param col Fill color
	 */
	public Rectangle(Point p, String col) {
		super(p, col);
	}

	
	/**
	 * Calculate and return the width of the rectangle.
	 * If both points are present, width = x2 - x1
	 * Otherwise, width is -1
	 * @return The width of the rectangle
	 */
	public double getWidth() {
		if(this.points[1] != null) {
			return (this.points[1].getXcord() - this.points[0].getXcord());			
		}
		return -1;
	}

	/**
	 * Calculate and return the height of a rectangle.
	 * If rectangle has both points, then height is y2 - y1
	 * Otherwise, rectangle's height is -1 
	 * @return The height of the rectangle
	 */
	public double getHeight() {
		if(this.points[1] != null) {
			return this.points[1].getYcord() - this.points[0].getYcord();
		}
		return -1;
	}
	
	/**
	 * Calculate and return the area of a rectangle
	 * If both height and width are > -1, calculate circumference
	 * Otherwise, return circumference as -1
	 * @see dt062g.lobe1602.assignment2.Shape#getCircumference()
	 */
	@Override
	public double getCircumference() {
		if(getHeight() >= 0 && getWidth() >= 0) {
			return ((getHeight() + getWidth()) * 2);
		}
		return -1;
	}

	/**
	 * If circumference is successfully calculated, then get the height
	 * Otherwise, height is -1
	 * @see dt062g.lobe1602.assignment2.Shape#getArea()
	 */
	@Override
	public double getArea() {
		if(getCircumference() >= 0) {
			return this.getHeight() * this.getWidth();
		}
		return -1;
	}
	

	/**
	 * @see dt062g.lobe1602.assignment2.Shape#draw()
	 */
	@Override
	public void draw() {
		System.out.println("Drawing a " + this.toString());
	}

	/**
	 * @see dt062g.lobe1602.assignment2.Shape#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {}
	
	/**
	 * Define a toString method for Rectangle object
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
	
		if(this.points[1] != null) {
			return  "Rectangle[start=" + this.points[0] + "; end=" + this.points[1] + 
					"; width=" + this.getWidth() + "; height=" + this.getHeight() + "; color=" + this.getColor() + "]";
			
		}
		else {
			return "Rectangle[start=" + this.points[0] + "; "
					+ "end=N/A; width=N/A; height=N/A; " + "color=" + this.getColor() + "]";
		
		}
	}
}
