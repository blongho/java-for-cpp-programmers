/**
 * <h1>Rectangle</h1>
 * Rectangle.java
 * @author	Bernard Che Longho (lobe1602)
 * @version	1.0
 * @since	2017-11-15
 * 
*/
package dt062g.lobe1602.assignment3;

import java.awt.Graphics;

/**
 * Rectangle class inherits from <code>Shape</code> This class has no instance
 * variables. It gets its coordinates from <code>Point</code>
 */
public class Rectangle extends Shape {

	/**
	 * Default contructor1
	 * 
	 * @param x The x-coordinate
	 * @param y The y-coordinate
	 * @param col The fill color
	 */
	public Rectangle(double x, double y, String col) {
		super(x, y, col);
	}

	/**
	 * Default constructor2
	 * 
	 * @param p <code>Point</code> in the x-y plane
	 * @param col Fill color
	 */
	public Rectangle(Point p, String col) {
		super(p, col);
	}

	/**
	 * Calculate and return the width of the rectangle. If both points are
	 * present, width = x2 - x1 Otherwise, throw an exception
	 * 
	 * @return The width of the rectangle
	 * @throws NoEndPointException if second point is absent
	 */
	public double getWidth() throws NoEndPointException {
		if (this.points[1] != null) {
			return (this.points[1].getXcord() - this.points[0].getXcord());
		} else {
			throw new NoEndPointException(
					"Triangle's width cannot be calculated, end point missing!");
		}
	}

	/**
	 * Calculate and return the height of a rectangle. If rectangle has both
	 * points, then height is y2 - y1 Otherwise, throws an exception
	 * 
	 * @return The height of the rectangle
	 * @throws NoEndPointException if second point is not provided
	 */
	public double getHeight() throws NoEndPointException {
		if (this.points[1] != null) {
			return this.points[1].getYcord() - this.points[0].getYcord();
		} else {
			throw new NoEndPointException(
					"Triangle's height cannot be calculated, end point missing!");
		}
	}

	/**
	 * Calculate and return the area of a rectangle If both height and width are
	 * defined calculate circumference Otherwise,throw an exception saying that
	 * no end point is found
	 * 
	 * @see dt062g.lobe1602.assignment3.Shape#getCircumference()
	 * @throws NoEndPointException if no end point.
	 */
	@Override
	public double getCircumference() throws NoEndPointException {
		if (points[1] != null) {
			return ((getHeight() + getWidth()) * 2);
		} else {
			throw new NoEndPointException(
					"Triangle's circumference cannot be calculated, end point missing!");
		}
	}

	/**
	 * @see dt062g.lobe1602.assignment3.Shape#getArea()
	 * @throws NoEndPointException An exception is thrown if height is or width
	 * is undefined due to lack of second point
	 */
	@Override
	public double getArea() throws NoEndPointException {
		if (points[1] != null) {
			return this.getHeight() * this.getWidth();
		} else {
			throw new NoEndPointException(
					"The triangle's area cannot be calculated, end point is missing!");
		}
	}

	/**
	 * @see dt062g.lobe1602.assignment3.Shape#draw()
	 */
	@Override
	public void draw() {
		System.out.println("Drawing a " + this.toString());
	}

	/**
	 * @see dt062g.lobe1602.assignment3.Shape#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {
	}

	/**
	 * Define a toString method for Rectangle object
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return this.getClass().getSimpleName() + "[start=" + this.points[0]
					+ "; end=" + this.points[1] + "; width=" + this.getWidth()
					+ "; height=" + this.getHeight() + "; color="
					+ this.getColor() + "]";
		} catch (NoEndPointException e) {// Catch and do nothing with the
										 // exception but return message of
										 // interest
			return this.getClass().getSimpleName() + "[start=" + this.points[0]
					+ "; " + "end=N/A; width=N/A; height=N/A; " + "color="
					+ this.getColor() + "]";
		}
	}
}
