package dt062g.lobe1602.assignment3;

/**
 * <h1>Assignment 3</h1> This application creates different shapes and calls
 * various methods to print circumference, print area and draw the shapes to the
 * standard output.
 *
 * @author Bernard Che Longho (lobe1602)
 * @version 1.0
 * @since 2017-11-22
 */
public class Assignment3 {

	/**
	 * @param args
	 *            No arguments are required for this program to work
	 * @throws NoEndPointException
	 *             This program throws an exception if no end point is provided
	 *             for a shape
	 */
	public static void main(String[] args) throws NoEndPointException {
		testRectangle();
		System.out.println(); // new line
		testCircle();
	}

	private static void testRectangle() throws NoEndPointException {
		// Create a rectangle and draw it.
		Rectangle r1 = new Rectangle(new Point(0, 0), "blue");
		System.out.println("Drawing a newly created rectangle...");
		r1.draw();

		// Print area of the rectangle.
		System.out.println();
		printArea(r1);

		// Set new end point to the rectangle by calling addPoint
		// with a new value and then print the area again.
		Point p1 = new Point(5, 5);
		System.out
				.println("\nChanging end point of rectangle to " + p1 + "...");
		r1.addPoint(p1);
		printArea(r1);

	}

	private static void testCircle() throws NoEndPointException {
		// Create a circle and draw it.
		Circle s1 = new Circle(5, 5, "black");
		System.out.println("Drawing a newly created circle...");
		s1.draw();

		// Print area of the circle.
		System.out.println();
		printArea(s1);

		// Set new end point to the rectangle by calling addPoint
		// with a new value and then print the area again.
		Point p1 = new Point(8, 9);
		System.out.println("\nChanging end point of circle to " + p1 + "...");
		s1.addPoint(p1);
		printArea(s1);
	}

	private static void printArea(Shape shape) throws NoEndPointException {
		System.out.println(
				"Printing area of a " + shape.getClass().getSimpleName());

		// Get area of the shape and print it.
		try {
			double area = shape.getArea();
			System.out.println("The area is: " + area);
		} catch (NoEndPointException e) {
			System.err.println(e.getMessage());
		}
	}
}