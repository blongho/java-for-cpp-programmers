/**
 * @file : Painting.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-03
 * @version : 1.0
 */
package dt062g.lobe1602.assignment6;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 * @author Bernard Che Longho (lobe1602)
 * 
 */
public class Painting extends JFrame implements ActionListener {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -7471358965925057123L;

	private JTextArea output;
	private JMenu fileMenu, editMenu;
	private JMenuBar menuBar;
	private JScrollPane scrollPane;
	private JComboBox<String> comboBox;
	private JMenuItem newMenuItem, saveMenuItem, loadMenuItem, exitMenuItem,
			undoMenuItem, nameMenuItem, authorMenuItem;
	private JPanel statusBar;
	private JLabel colorDisplay;
	private JLabel coordinates;
	private JPanel colorShapePanel;
	private String filename;
	private String windowTitle;
	private String author;
	private String name;
	private Drawing drawing;
	private Point point;

	/**
	 * @throws HeadlessException
	 */
	public Painting() throws HeadlessException {
		super();
		name = author = filename = "";
		windowTitle = this.getClass().getSimpleName();
		drawing = new Drawing(name, author);
		setLayout(new BorderLayout());

		setTitle(windowTitle);
		ImageIcon icon = new ImageIcon("img/shapes.png");
		setIconImage(icon.getImage());

		scrollPane = makeDisplayArea();

		menuBar = createMenuBar();

		setJMenuBar(menuBar);

		colorShapePanel = makeColorShapePanel();

		add(colorShapePanel, BorderLayout.PAGE_START);
		add(scrollPane, BorderLayout.CENTER);
		JPanel status = createStatusBar();
		add(status, BorderLayout.PAGE_END);
		// add(new StatusBar(), BorderLayout.PAGE_END);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);

	}

	/**
	 * Make a display area and add mouseClick event to it
	 * 
	 * @return JScrollPane object with the text area
	 */
	private JScrollPane makeDisplayArea() {
		output = new JTextArea(10, 50);
		output.setMargin(new Insets(10, 10, 10, 10));
		output.setEditable(false); // display text needs not be edited by
								   // the user
		output.setToolTipText("Stuff will be displayed here");
		output.setBackground(Color.white);
		output.setOpaque(true);
		output.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				point = new Point(e.getX(), e.getY());
				updateCoordinates(point.toString());
			}
		});
		scrollPane = new JScrollPane(output);
		return scrollPane;
	}

	/**
	 * Create menu for the file loader
	 * 
	 * @return File submenu. This will be called in the main program
	 */
	private JMenuBar createMenuBar() {
		menuBar = new JMenuBar();
		fileMenu = new JMenu("File");
		fileMenu.setMnemonic(KeyEvent.VK_F);
		fileMenu.setToolTipText("Here you can manage files");

		newMenuItem = menuItem("New...", new ImageIcon("img/file.png"),
				KeyEvent.VK_N, true);
		newMenuItem.setToolTipText("Create a new file");

		saveMenuItem = menuItem("Save as...", new ImageIcon("img/save.gif"),
				KeyEvent.VK_S, true);
		saveMenuItem.setToolTipText("Save current drawing");

		loadMenuItem = menuItem("Load...", new ImageIcon("img/open.gif"),
				KeyEvent.VK_O, true);
		loadMenuItem.setToolTipText("Load image from file");

		exitMenuItem = menuItem("Exit", new ImageIcon("img/exit.png"),
				KeyEvent.VK_X, true);
		exitMenuItem.setToolTipText("Exits the program");

		// add file menu items
		fileMenu.add(newMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(loadMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);
		// System.out.println(fileMenu.getMenuComponentCount());

		menuBar.add(fileMenu);

		editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		editMenu.setToolTipText("Do some editing concerning a drawing");

		undoMenuItem = menuItem("Undo", new ImageIcon("img/undo.png"),
				KeyEvent.VK_U, false);
		undoMenuItem.setToolTipText("Undo changes (Does nothing for now!)");

		nameMenuItem = menuItem("Name...", new ImageIcon("img/name.png"),
				KeyEvent.VK_N, true);
		nameMenuItem.setToolTipText("Update the name of the drawing");

		authorMenuItem = menuItem("Author...", new ImageIcon("img/author.png"),
				KeyEvent.VK_A, true);
		authorMenuItem.setToolTipText("Update the author of the drawing");
		editMenu.add(undoMenuItem);
		editMenu.add(nameMenuItem);
		editMenu.add(authorMenuItem);

		menuBar.add(editMenu);

		menuBar.setVisible(true);

		return menuBar;

	}

	/**
	 * A small snippet to quickly make and add a menuItem to the main menu
	 * 
	 * @param title
	 *            The display text of the menu item
	 * @param image
	 *            The icon to display along-side the menu text
	 * @param hotkey
	 *            The keyboard short-cut to reach this menu item
	 * @return JMenuItem object
	 */
	private JMenuItem menuItem(String title, ImageIcon image, int hotkey,
			boolean action) {
		JMenuItem jMenuItem = new JMenuItem(title, image);
		jMenuItem.setHorizontalTextPosition(SwingConstants.RIGHT);
		jMenuItem.setMnemonic(hotkey);
		if (action) {
			jMenuItem.addActionListener(this);
		}
		return jMenuItem;

	}

	/**
	 * Build the color chooser and combo pane panel
	 * 
	 * @return JPanel object , the color and combobox selection pane
	 */
	private JPanel makeColorShapePanel() {
		JPanel green, red, yellow, blue, black, cyan;
		green = new JPanel();
		red = new JPanel();
		yellow = new JPanel();
		blue = new JPanel();
		black = new JPanel();
		cyan = new JPanel();

		Vector<JPanel> lables = new Vector<JPanel>();
		lables.addElement(green);
		lables.addElement(red);
		lables.addElement(yellow);
		lables.addElement(blue);
		lables.addElement(black);
		lables.addElement(cyan);

		green.setBackground(Color.green);
		red.setBackground(Color.red);
		yellow.setBackground(Color.yellow);
		blue.setBackground(Color.blue);
		black.setBackground(Color.black);
		cyan.setBackground(Color.cyan);

		for (JPanel jLabel : lables) {
			jLabel.setOpaque(true);

			// give each jlabel object a mouseClick events
			jLabel.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					if (e.getSource() == green) {
						colorDisplay.setBackground(Color.green);
					} else if (e.getSource() == red) {
						colorDisplay.setBackground(Color.red);
					} else if (e.getSource() == yellow) {
						colorDisplay.setBackground(Color.yellow);
					} else if (e.getSource() == blue) {
						colorDisplay.setBackground(Color.blue);
					} else if (e.getSource() == black) {
						colorDisplay.setBackground(Color.black);
					} else if (e.getSource() == cyan) {
						colorDisplay.setBackground(Color.cyan);
					}
				}
			});
		}

		colorShapePanel = new JPanel();
		colorShapePanel.setLayout(new GridLayout(1, 7));
		colorShapePanel.setPreferredSize(new Dimension(100, 30));

		for (JPanel jLabel : lables) {
			colorShapePanel.add(jLabel);
		}

		// System.out.println(colorShapePanel.getSize());
		// Create the combobox
		comboBox = new JComboBox<>();
		comboBox.addItem("Rectangle");
		comboBox.addItem("Circle");
		comboBox.setEditable(false);
		comboBox.setSelectedItem("Circle");

		comboBox.setMinimumSize(new Dimension(30, 15));
		comboBox.setMaximumSize(comboBox.getMinimumSize());

		colorShapePanel.add(comboBox);

		colorShapePanel.setVisible(true);
		return colorShapePanel;
	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == undoMenuItem) {
			return;
		} else if (e.getSource() == nameMenuItem) {
			setDrawingName();

		} else if (e.getSource() == authorMenuItem) {
			setDrawingAuthor();

		} else if (e.getSource() == saveMenuItem) {
			String sample = "";
			if (drawing.getAuthor() == "" && drawing.getName() == "") {
				sample = ".xml";
			} else if (drawing.getAuthor() == "" && drawing.getName() != "") {
				sample = drawing.getName() + ".xml";
			} else if (drawing.getAuthor() != "" && drawing.getName() != "") {
				sample = drawing.getName() + " by " + drawing.getAuthor()
						+ ".xml";
			}

			filename = (String) JOptionPane.showInputDialog(null,
					"Save drawing to...", "File name",
					JOptionPane.QUESTION_MESSAGE, new ImageIcon("img/file.png"),
					null, sample);

		} else if (e.getSource() == loadMenuItem) {
			filename = JOptionPane.showInputDialog(null, "Load drawing from...",
					"File to upload", JOptionPane.QUESTION_MESSAGE);

		} else if (e.getSource() == newMenuItem) {
			setDrawingName();
			setDrawingAuthor();
		} else if (e.getSource() == exitMenuItem) {
			System.exit(0);
		}
	}

	/**
	 * Create the status bar <br />
	 * 
	 * @return the frame's status bar
	 */
	private JPanel createStatusBar() {
		BorderLayout borderLayout = new BorderLayout();

		statusBar = new JPanel(borderLayout);
		coordinates = new JLabel("Coordinates: 0.0", SwingConstants.LEFT);
		statusBar.add(coordinates, BorderLayout.WEST);

		JPanel colorPane = new JPanel();
		JLabel selectText = new JLabel("Selected color : ");
		colorPane.add(selectText, BorderLayout.WEST);

		colorDisplay = new JLabel();
		colorDisplay.setOpaque(true);
		colorDisplay.setBackground(Color.gray);
		colorDisplay.setPreferredSize(new Dimension(20, 20));
		colorDisplay.setBorder(BorderFactory.createRaisedBevelBorder());

		colorPane.add(colorDisplay, BorderLayout.EAST);
		colorPane.setBackground(Color.gray);
		statusBar.setBackground(Color.gray);
		statusBar.add(colorPane, BorderLayout.EAST);
		statusBar.setOpaque(true);

		return statusBar;
	}

	/**
	 * Update the value of coordinates when user clicks a point on the screen
	 * 
	 * @param text
	 *            A point object returned to string
	 */
	private void updateCoordinates(String text) {
		String string[] = coordinates.getText().split(" ");
		coordinates.setText(string[0] + " " + text);
	}

	/**
	 * Dynamically update the window title as the user enters values for the
	 * painting (name) and author
	 */
	private void updateWindowTitle() {
		String tmp = "";
		if (!author.isEmpty() && !name.isEmpty()) {
			tmp = name + " by " + author;
		} else if (!author.isEmpty() && name.isEmpty()) {
			tmp = author;
		} else if (author.isEmpty() && !name.isEmpty()) {
			tmp = name;
		}
		// setTitle(windowTitle + " - " + tmp);
		if (!author.isEmpty() || !name.isEmpty()) {
			setTitle(windowTitle + " - " + tmp);
		}
	}

	/**
	 * Let the user enter the new name for the drawing or edit it
	 */
	private void setDrawingName() {
		name = JOptionPane.showInputDialog(null,
				"Enter the name of the drawing", "Name of drawing",
				JOptionPane.QUESTION_MESSAGE);
		drawing.setName(name);
		updateWindowTitle();
	}

	/**
	 * Let the user enter a new author name or edit the current one
	 */
	private void setDrawingAuthor() {
		author = JOptionPane.showInputDialog(null, "Enter the author name",
				"Author's name", JOptionPane.QUESTION_MESSAGE);
		drawing.setAuthor(author);
		updateWindowTitle();
	}
}