/**
 * <h1>Drawing</h1> The Drawing class enables the drawing of shapes including
 * the shape name and the author who made the drawing
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.2
 * @since 2017-12-11
 * 
 */
package dt062g.lobe1602.assignment6;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Bernard Che Longho (lobe1602) A class that draws objects and some
 *         details
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Drawing implements Drawable {

	/** The name of the shape */
	@XmlElement
	private String name = "";

	/** The artist who drew the shape */
	@XmlElement
	private String author = "";

	/** The container to hold the shapes */
	@XmlElement(name = "shape")
	private ArrayList<Shape> shapes; // container to hold shapes

	/**
	 * Empty default construction for drawing
	 */
	public Drawing() {
		shapes = new ArrayList<>();
	}

	/**
	 * Parameterised constructor with shape name and author name
	 * 
	 * @param name
	 *            Shape name
	 * @param author
	 *            Author name
	 */
	public Drawing(String name, String author) {
		shapes = new ArrayList<>();
		this.name = name;
		this.author = author;
	}

	/**
	 * If there are shapes, produce a drawing. <br>
	 * If there are no shapes, tell user that drawing is impossible
	 * 
	 * @see dt062g.lobe1602.assignment4.Drawable#draw()
	 */
	@Override
	public void draw() {
		if (!shapes.isEmpty()) {
			System.out.println("A drawing by " + author + " called " + name);
			for (Shape shape : shapes) {
				shape.draw();
			}
		} else {
			System.err.println(
					"Impossible to produce a drawing without shapes\n");
		}
	}

	/**
	 * @see dt062g.lobe1602.assignment4.Drawable#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub

	}

	/**
	 * Get the name of the shape
	 * 
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Set the name of the shape
	 * 
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Get the author's name
	 * 
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * Set the author's name
	 * 
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * Set the shapes that make up a drawing
	 * 
	 * @param shapes
	 *            the shapes that make up a drawing.
	 */
	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
	}

	/**
	 * Get all the shapes that make up a drawing
	 * 
	 * @return the shapes that make up a drawing
	 */
	public ArrayList<Shape> getShapes() {
		return shapes;
	}

	/**
	 * @param shape
	 *            Add a shape in the list of shapes
	 */
	public void addShape(Shape shape) throws NullPointerException {
		if (shape != null) {
			shapes.add(shape);
		} else {
			throw new NullPointerException("Adding a null shape is forbidden!");
		}
	}

	/**
	 * @return the number of shapes in <code>shapes</code>
	 */
	public int getSize() {
		return shapes.size();
	}

	/**
	 * @return total circumference of shapes in the container
	 * @throws NoEndPointException
	 *             if the circumference of a shape can not be calculated
	 */
	public double getTotalCircumference() throws NoEndPointException {
		double totalCircumference = 0.0;
		try {
			for (Shape shape : shapes) {
				totalCircumference += shape.getCircumference();
			}
		} catch (NoEndPointException e) {
		}
		return totalCircumference;
	}

	/**
	 * @return total area of all the shapes in <code>shapes</code> <br>
	 *         If the area cannot be calculated, an exception is thrown and
	 *         caught here. Only shapes whose area can be calculated are
	 *         included in the calculations
	 */
	public double getTotalArea() throws NoEndPointException {
		double totalarea = 0.0;
		try {
			for (Shape shape : shapes) {
				totalarea += shape.getArea();
			}
		} catch (NoEndPointException e) {
		}
		return totalarea;
	}

	/**
	 * Reset everything to the starting state i.e author's name and shape name
	 * should be set to empty and the shapes size to zero
	 */
	public void clear() {
		name = "";
		author = "";
		shapes.clear();
	}

	/**
	 * @see java.lang.Object#toString() <br>
	 *      Printing a figure,
	 *      <ul>
	 *      <li>the name of the figure</li>
	 *      <li>the author of the figure</li>
	 *      <li>the size of the figure (i.e how many shapes are combined to make
	 *      the shape)</li> and
	 *      <li>the total area and circumference of the shapes that make the
	 *      figure</li>
	 */
	@Override
	public String toString() {

		double area = 0;
		double circumference = 0;

		if (!shapes.isEmpty()) {
			try {
				area = getTotalArea();
				circumference = getTotalCircumference();
			} catch (NoEndPointException | IndexOutOfBoundsException e) {
			}
		}

		return String.format(
				"Drawing[name=%s; author=%s; size=%d; circumference=%.1f; area=%.1f]",
				name, author, getSize(), circumference, area);
	}

}
