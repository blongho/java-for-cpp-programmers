/**
 * <h1>Point</h1> A class to represent x and y coordinates of any object(shape)
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.4
 * @since 2017-01-04
 */
package dt062g.lobe1602.assignment7;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * <code>Point</code> class declared public as it is designed to be used by
 * other classes
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
public class Point {
	@XmlElement
	private double x;
	/** The x-coordinate */
	@XmlElement
	private double y;
	/** The y-coordinate */

	// Constructors
	/**
	 * Default constructor of <code>Point</code> with default values for x and y
	 * coordinates set to 0 (zero)
	 */
	public Point() {
		x = y = 0;
	}

	/**
	 * Default construct for <code>Point</code> with values set for the x and y
	 * coordinates
	 * 
	 * @param x
	 *            The value of the x-coordinate
	 * @param y
	 *            The value of the y-coordinate
	 */
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}

	// Methods
	/**
	 * Get the value of the x-coordinate
	 * 
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * Set a new value for the x-coordinate
	 * 
	 * @param x
	 *            the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * Return the y coordinate
	 * 
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * Set a new value for the y coordinate
	 * 
	 * @param y
	 *            the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * Overload the toString method to allow a representation of a
	 * <code>Point</code> object as a string.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (int) this.x + ", " + (int) this.y;
	}
}