/**
 * <h1>Drawable</h1> The graphical interface for printing(drawing) shapes on the
 * screen
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.0
 * @since 2017-11-30
 */
package dt062g.lobe1602.assignment7;

import java.awt.Graphics;

/**
 * Interface class declaring methods for shapes drawing
 */
public interface Drawable {
	/**
	 * Empty draw method
	 */
	void draw();

	/**
	 * Draw a graphic structure on the screen
	 * 
	 * @param g
	 *            The coordinates of the shape to be drawn
	 */
	void draw(Graphics g);
}
