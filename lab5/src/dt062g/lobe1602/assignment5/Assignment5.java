/**
 * <h1>Assignment 5</h1> This application creates a <code>Drawing</code> with a
 * name, author and different shapes in it. It then saves the drawing to XML,
 * clear the drawing and then loads a drawing from XML.
 *
 * @author Bernard Che Longho
 * @version 1.0
 * @since 2017-12-11
 */
package dt062g.lobe1602.assignment5;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.bind.JAXBException;

/**
 * Test program for assignment 5
 * 
 */
public class Assignment5 {

	/**
	 * The main program where everything is run
	 * 
	 * @param args
	 *            No arguments are needed for this program to run
	 * @throws JAXBException
	 *             If there is error parsing the file, an error will be thrown
	 * @throws IOException
	 *             If there is an error writing to or reading from a file, an
	 *             IOException will be thrown
	 */
	public static void main(String[] args) throws JAXBException, IOException {
		testDrawing();
	}

	private static void testDrawing() {
		// Create a drawing with a name and author.
		System.out.println("Create a drawing...\n");
		Drawing d1 = new Drawing();

		// Create at least 5 shapes with end points

		Rectangle rec1 = new Rectangle(2, 4, "#0059b3");
		Rectangle rec2 = new Rectangle(new Point(5, 3), "#ff0000");
		Circle circ1 = new Circle(5, 10, "#ffffff");
		Circle circ2 = new Circle(12, 33, "#ffff00");
		Rectangle rec3 = new Rectangle(15, 20, "#008000");
		rec1.addPoint(12, 20);
		rec2.addPoint(new Point(23, 44));
		rec3.addPoint(23, 33);
		circ2.addPoint(new Point(23, 33));
		circ1.addPoint(22, 33);

		// Add shapes to the drawing
		ArrayList<Shape> shapes = new ArrayList<>();
		shapes.add(rec1);
		shapes.add(rec2);
		shapes.add(rec3);
		shapes.add(circ2);
		shapes.add(circ1);

		d1.setShapes(shapes);
		d1.setAuthor("Bernard");
		d1.setName("Jujukalaba");

		// Print the drawing
		d1.draw();

		// Save the drawing to XML
		final String fileName = "";
		String derived = "'" + d1.getName() + " by " + d1.getAuthor() + ".xml'";

		String goodFile = FileHandler.validFileName(fileName);

		System.out.println("\nSave the drawing to "
				+ (goodFile.isEmpty() ? derived : goodFile) + "...");

		try {
			if (goodFile.isEmpty()) {
				FileHandler.saveToXML(d1);
			} else {
				FileHandler.saveToXML(d1, goodFile);
			}
		} catch (JAXBException e) {
			System.err.println("JAXB error: " + e.getMessage());
		} catch (IOException e2) {
			System.err.println("I/O error: " + e2.getMessage());
		}

		// // Load the drawing and print
		// try {
		// d1 = FileHandler.loadFromXML("Jujukalaba by Bernard.xml");
		// FileHandler.showXML(d1);
		// } catch (JAXBException e) {
		// System.err.println("JAXB error: " + e.getMessage());
		// } catch (IOException e2) {
		// System.err.println("I/O error: " + e2.getMessage());
		// }

		// Clear and print
		System.out.println("\nClearing the drawing and then draw it....");
		d1.clear();
		d1.draw();

		Drawing d2 = new Drawing();
		String file2 = "Mona Lisa.xml";
		String validFile2 = FileHandler.validFileName(file2);
		// Load drawing from XML
		System.out.println("\nLoad drawing from " + validFile2 + " ...\n");
		try {
			d2 = FileHandler.loadFromXML(validFile2);
		} catch (JAXBException e) {
			System.err.println("JAXB error: " + e.getMessage());
		} catch (IOException e2) {
			System.err.println("I/O error: " + e2.getMessage());
		}

		// Print the drawing
		d2.draw();
		try {
			FileHandler.showXML(d2);
		} catch (JAXBException e) {
			System.err.println("JAXBE error : " + e.getMessage());
		}
	}
}