/**
 * <h1>Shape</h1> An abstract class which will be a superclass for different
 * geometric shapes. It implements the <code>Drawable</code>
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.2
 * @since 2017-11-30
 */
package dt062g.lobe1602.assignment5;

import java.awt.Graphics;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * <code>Shape</code> Abstract class for drawing shapes on the screen
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement
@XmlSeeAlso({ Rectangle.class, Circle.class })
public abstract class Shape implements Drawable {
	/** Fill color */
	@XmlElement
	protected String color;

	/** Array of Point objects */
	@XmlElement(name = "point")
	protected ArrayList<Point> points = new ArrayList<>();

	/**
	 * Default constructor1 of <code>Shape</code> that takes the x and y values
	 * of a point and the color to fill the shape
	 * 
	 * @param x
	 *            The x-coordinate of a point in the shape
	 * @param y
	 *            The y-coordinate of a point in the shape
	 * @param col
	 *            The color to fill the shape
	 */
	public Shape(double x, double y, String color) {
		points.add(0, new Point(x, y));
		this.color = color;
	}

	/**
	 * Second default constructor of Shape. Takes a <code>Point</code> object
	 * and the color to fill a shape
	 * 
	 * @param p
	 *            A point object
	 * @param col
	 *            Color to fill a shape
	 */
	public Shape(Point p, String color) {
		points.add(0, p);
		this.color = color;
	}

	/**
	 * 
	 */
	public Shape() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see dt062g.lobe1602.assignment4.Drawable#draw()
	 */
	@Override
	public abstract void draw();

	/**
	 * @see dt062g.lobe1602.assignment4.Drawable#draw(java.awt.Graphics)
	 */
	@Override
	public abstract void draw(Graphics g);

	/**
	 * The the current fill color
	 * 
	 * @return the color
	 */
	public final String getColor() {
		return this.color;
	}

	/**
	 * Set a new fill color
	 * 
	 * @param color
	 *            the color to set
	 */
	public void setColor(final String color) {
		this.color = color;
	}

	/**
	 * Set the points
	 * 
	 * @param points
	 *            Will be set the the points of this shape
	 */
	public void setPoint(ArrayList<Point> points) {
		this.points = points;
	}

	/**
	 * @return points The points of the shape
	 */
	public ArrayList<Point> getPoints() {
		return points;
	}

	/**
	 * @return the circumference of a shape An exception will be thrown if the
	 *         end point of a shape is not provided
	 */
	public abstract double getCircumference() throws NoEndPointException;

	/**
	 * @return the area of a shape An exception will be thrown if the end point
	 *         of a shape is not provided
	 */
	public abstract double getArea() throws NoEndPointException;

	/**
	 * Add a point to the second position of {@link #points} from a Point object
	 * 
	 * @param p
	 *            Point object to add
	 */
	public void addPoint(Point p) {
		points.add(1, p);
	}

	/**
	 * Add a point to the second position of {@link #points} by creating a point
	 * object using provided x- and y-coordinates
	 * 
	 * @param x
	 *            a point's x-coordinate
	 * @param y
	 *            a point's y-coordinate
	 */
	public void addPoint(double x, double y) {
		points.add(1, new Point(x, y));
	}
}