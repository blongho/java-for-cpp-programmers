/**
 * @file : FileHandler.java <br>
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2017-12-11
 * @version : 1.0
 */
package dt062g.lobe1602.assignment5;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 * FileHandler handles the saving of shapes as xml files and reading of same
 * shapes from xml file
 */
public class FileHandler {

	private final static String fileExtension = ".xml";

	/**
	 * Default constructor
	 */
	public FileHandler() {
	}

	/**
	 * Save a drawing to a user-defined XML file <br>
	 * - Check that the filename ends with .xml, otherwise, append .xml to the
	 * name
	 * 
	 * @param drawing
	 *            The drawing to save to file
	 * @param filename
	 *            The name of the file on which the drawing will be saved
	 * @catch FileNotFoundException if file not found
	 */
	static void saveToXML(Drawing drawing, String filename)
			throws JAXBException, IOException {
		String filn = validFileName(filename);
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Drawing.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(drawing, new File(filn));
			showXML(drawing);
		} catch (JAXBException e) {
			throw new JAXBException(
					"There were some errors parsing saving the xml file");
		}
	}

	/**
	 * Save a drawing to an XML file. No file name provided. <br>
	 * FileName is auto-generated to be "name by author.xml" <br>
	 * where name= name of the drawing, author = author of the drawing
	 * 
	 * @param drawing
	 *            The drawing to save to file
	 */
	static void saveToXML(Drawing drawing) throws JAXBException, IOException {
		String filname = drawing.getName() + " by " + drawing.getAuthor()
				+ fileExtension;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(Drawing.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.marshal(drawing, new File(filname));
			showXML(drawing);
		} catch (JAXBException e) {
			throw new JAXBException(
					"There were some errors parsing saving the xml file");
		}
	}

	/**
	 * Load a xml document from file
	 * 
	 * @param filename
	 *            Filename with document to load
	 * @throws JAXBException
	 */
	static Drawing loadFromXML(String filename)
			throws JAXBException, IOException {

		Drawing drawing = new Drawing();
		JAXBContext jaxbContext = JAXBContext.newInstance(Drawing.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		drawing = (Drawing) unmarshaller
				.unmarshal(new File(validFileName(filename)));

		return drawing;
	}

	/**
	 * Display well-formatted xml file
	 * 
	 * @param drawing
	 *            The drawing to be displayed
	 * @throws JAXBException
	 */
	static void showXML(Drawing drawing) throws JAXBException {
		JAXBContext jaxbContext;
		try {
			jaxbContext = JAXBContext.newInstance(Drawing.class);
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

			System.out.println(
					"\n====================Welformed xml ============================");
			marshaller.marshal(drawing, System.out);
			System.out.println("================ END =====================");

		} catch (JAXBException e) {
			throw new JAXBException("Oops! Error occured parsing the xml file");
		}

	}

	/**
	 * Make sure file name as a .xml extension<br>
	 * Append .xml to file name if the file name does not already have it
	 * 
	 * @param filename
	 *            User-given filename
	 * @return File name with .xml ending
	 */
	public static String validFileName(String filename) {
		if (filename.isEmpty()) {
			return "";
		} else if (filename.endsWith(fileExtension)) {
			return filename;
		} else {
			return filename += fileExtension;
		}
	}
}
