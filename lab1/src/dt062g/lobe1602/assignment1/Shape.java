package dt062g.lobe1602.assignment1;
/**
 * <h1>Shape </h1>
 * <p>
 * This class is the interface for the calculations. 
 *
 *<p>
 * The interface class. Contains method declarations for 
 * <ul>
 * 	<li>{@code shapeName()} </li>
 * <li>{@code area()} </li>
 * <li>{@code Circumference()} </li>
 * </ul>
 * The {@code Triangle} and {@code Circle} classes will need this for their own implementation
 *
 *
 * @author Bernard Che Longho (lobe1602)
 * @since 2017-11-09
 * @version 1.0
 *
 */
public interface Shape {
	public double area();
	public double circumference();
}
