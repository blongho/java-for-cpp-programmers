/**
 * <h1>Assignment 1</h1>
 * This program allows the user to enter data for a circle
 * or rectangle. The circumference and area are then calculated
 * and the result is displayed to the standard output.
 * <p>
 * @author	Bernard Che Longho (lobe1602)
 * @version	1.0
 * @since	2017-11-10
 */
package dt062g.lobe1602.assignment1;


public class TestCalculator {

	/**
	 * @param args No arguments are required for this program to run
	 */
	public static void main(String[] args) {
		MakeCalculation mc = new MakeCalculation();
		mc.run();
		}
	}
