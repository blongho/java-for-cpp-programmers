package dt062g.lobe1602.assignment1;
/**
 * <h1>Circle </h1>
 * <p>
 * A circle inherits from {@code Shape}
 * 
 * <p>
 * This class contains features related to a circle i.e {@code radius} and {@code pi} <br>
 * which are used for the calculation of area and circumference
 * 
 **@author Bernard Che Longho
 * @since 2017-11-09
 * @version 0.1
 *
 */
public class Circle implements Shape {
    private double radius;
	private static final double pi = 3.14;
	
	
	/**
	 * Default constructor for the Circle
	 * @param radius The radius of the circle
	 */
	public Circle(final double radius){
		this.radius = radius;
	}
	

	/**
	 * @return the radius
	 */
	public final double getRadius() {
		return radius;
	}

	/**
	 * {@code setRatius()}
	 * @param radius the radius to set
	 */
	public void setRadius(final double radius) {
		this.radius = radius;
	}


	/**
	 * Calculate the area of the circle
	 * @return Circle area
	 */
	@Override
	public final double area() {
		return  pi * radius * radius;
	}


	/**
	 * Calculate the circumference of the circle
	 * @return The circle circumference
	 */
	@Override
	public final double circumference() {
		return 2 * pi * radius;
	}
}
