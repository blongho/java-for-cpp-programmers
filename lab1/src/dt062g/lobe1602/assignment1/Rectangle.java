package dt062g.lobe1602.assignment1;

/**
 * <h1> Rectangle calculations </h1>
 * <p>
 * The rectangle class contains stuff related to the triangle such as
 * <ul><li>{@code lenght}</li>
 * 		<li>{@code width}</li>
 * </ul>
 * 
 * @author Bernard Che Longho (lobe1602)
 * @since 2017-11-09
 * @version 1.0
 */
public class Rectangle implements Shape {
	private int length;
	private int width;
	
	
	/**
	 * Constructor of the rectangle. 
	 * @param length	The length of the rectangle
	 * @param width		The width of the rectangle
	 */
	public Rectangle(final int length, final int width) {
		this.setLength(length);
		this.setWidth(width);
	}

	/**
	 * @return the length
	 */
	public final int getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(final int length) {
		this.length = length;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(final int width) {
		this.width = width;
	}
	
	/**
	 * Calculate the area of the rectangle
	 * @return The area of the rectangle (lenght * width)
	 */
	@Override
	public final double area() {
		return length * width;
	}
	
	/**
	 * Calculate the perimeter of the rectangle
	 * @return The perimeter (circumference) of the rectangle
	 */
	@Override
	public final double circumference() {
		return (length + width) * 2;
	}
	
}
