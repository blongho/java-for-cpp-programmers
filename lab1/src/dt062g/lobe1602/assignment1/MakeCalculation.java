package dt062g.lobe1602.assignment1;

import java.util.*;

/**
 * <h1>MakeCalculation </h1>
 * <p>
 * MakeCalculation is the class where the user interacts with the program. 
 * It is the place where inputs and outputs occur
 * 
 * @author Bernard Che Longho (lobe1602)
 * @since 2017-11-09
 * @version 1.0
 */

public class MakeCalculation {
	
	/**
	 * {@value choice} The shape whose area and circumference will be calculated
	 */
	private String choice = "";
	
	
	/**
	 * <p>Print the data from the calculations 
	 * <p> Summarize to exactly 2 decimal places
	 * @param shape The shape whose data is to be printed
	 */
	public final void showResult(final Shape shape) {
		System.out.println("Circumference = " + String.format( "%.2f", shape.circumference()));
		System.out.println("Area = " + String.format("%.2f", shape.area()));
	}
	
	/**
	 * <p> This method gets the kind of shape, 
	 * calculates the results and 
	 * displays it to the user
	 */
	public void calculate(){
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Welcome to the shapes calculator! Enter \"exit\" to quit the program.");
		
	loop: while(choice != "exit") {
					
			System.out.print("What geometric shape do you want to use? : ");
			
			choice = scan.nextLine();
			
			String input = "";
			
			switch(choice){
			
			case "rectangle":
				int length;
				int width;
				
				System.out.print("Enter width: ");
				
				input = scan.nextLine();
				
				width = Integer.parseInt(input);
				
				System.out.print("Enter height: ");
				
				input = scan.nextLine();
				
				length = Integer.parseInt(input);
								
				Shape rect = new Rectangle(length, width);
				
				showResult(rect);
				
				break;
				
			case "circle":
				System.out.print("Enter the radius: ");
				
				input = scan.nextLine();
				
				double radius = Double.parseDouble(input);
				
				Shape circ = new Circle(radius);
				
				showResult(circ);
				
				break;
				
			case "exit":
				System.out.println("Good bye!");
				break loop;
				
			default: 
				System.out.println("'" + choice + "'" + "is an unknown shape to this program!");
				break;
				} // END SWITCH
		
		} // END WHILE
		
		scan.close();
	} // END CALCULATE
	
	/**
	 * <p>Everything is hidden inside run.
	 * <p> Run calls calculate() what does all the magic 
	 */
	public void run() { calculate(); }
}
