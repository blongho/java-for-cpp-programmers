/**
 * <h1>NoEndPointException</h1> This is a customized exception class that prints
 * out a message informing the user that there is no end point when the user
 * attempts to get a value at an empty location in a <code>Point</code>
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.0
 * @since 2017-11-30
 */
package dt062g.lobe1602.assignment4;

/**
 * This class inherits from the java.lang.Exception class
 */
public class NoEndPointException extends Exception {

	/**
	 * serialVersionUID This id is added just to avoid a warning message from
	 * the editor. I have no idea why it is here. <b>I have told you </b>
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor
	 */
	public NoEndPointException() {
	}

	/**
	 * Parametarized constructor
	 * 
	 * @param message
	 *            The message to print out if an exception occurs This message
	 *            prints out the cause of the exception
	 */
	public NoEndPointException(String message) {
		super(message);
	}

}
