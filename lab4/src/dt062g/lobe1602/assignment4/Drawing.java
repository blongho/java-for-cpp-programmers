/**
 * <h1>Drawing</h1> The Drawing class enables the drawing of shapes including
 * the shape name and the author who made the drawing
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.0
 * @since 2017-11-30
 * 
 */
package dt062g.lobe1602.assignment4;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Bernard Che Longho (lobe1602) A class that draws objects and some
 *         details
 */
public class Drawing implements Drawable {

	private String name = ""; // the name of the shape
	private String author = ""; // artist who made the shape
	private List<Shape> shapes; // container to hold shapes

	/**
	 * Empty default construction for drawing
	 */
	public Drawing() {
		shapes = new ArrayList<>();
	}

	/**
	 * Parameterised constructor with shape name and author name
	 * 
	 * @param name
	 *            Shape name
	 * @param author
	 *            Author name
	 */
	public Drawing(String name, String author) {
		shapes = new ArrayList<>();
		this.name = name;
		this.author = author;
	}

	/**
	 * @see dt062g.lobe1602.assignment4.Drawable#draw()
	 */
	@Override
	public void draw() {
		System.out.println("A drawing by " + author + " called " + name);
		for (Shape shape : shapes) {
			shape.draw();
		}

	}

	/**
	 * @see dt062g.lobe1602.assignment4.Drawable#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @param shape
	 *            Add a shape in the list of shapes
	 */
	public void addShape(Shape shape) throws NullPointerException {
		if (shape != null) {
			shapes.add(shape);
		} else {
			throw new NullPointerException("Adding a null shape is forbidden!");
		}
	}

	/**
	 * @return the number of shapes in <code>shapes</code>
	 */
	public int getSize() {
		return shapes.size();
	}

	/**
	 * @return total circumference of shapes in the container
	 * @throws NoEndPointException
	 *             if the circumference of a shape can not be calculated
	 */
	public double getTotalCircumference() throws NoEndPointException {
		double totalCircumference = 0.0;
		try {
			for (Shape shape : shapes) {
				totalCircumference += shape.getCircumference();
			}
		} catch (NoEndPointException e) {
		}
		return totalCircumference;
	}

	/**
	 * @return total area of all the shapes in <code>shapes</code> <br>
	 *         If the area cannot be calculated, an exception is thrown and
	 *         caught here. Only shapes whose area can be calculated are
	 *         included in the calculations
	 */
	public double getTotalArea() throws NoEndPointException {
		double totalarea = 0.0;
		try {
			for (Shape shape : shapes) {
				totalarea += shape.getArea();
			}
		} catch (NoEndPointException e) {
		}
		return totalarea;
	}

	/**
	 * @see java.lang.Object#toString() <br>
	 *      Printing a figure,
	 *      <ul>
	 *      <li>the name of the figure</li>
	 *      <li>the author of the figure</li>
	 *      <li>the size of the figure (i.e how many shapes are combined to make
	 *      the shape)</li> and
	 *      <li>the total area and circumference of the shapes that make the
	 *      figure</li>
	 */
	@Override
	public String toString() {

		double area = 0;
		double circumference = 0;

		if (!shapes.isEmpty()) {
			try {
				area = getTotalArea();
				circumference = getTotalCircumference();
			} catch (NoEndPointException | IndexOutOfBoundsException e) {
			}
		}

		return String.format(
				"Drawing[name=%s; author=%s; size=%d; circumference=%.1f; area=%.1f]",
				name, author, getSize(), circumference, area);
	}

}
