/**
 * <h1>Circle</h1> This class contains the methods for manipulating a circle. It
 * inherits from the <code>Shape</code> class.
 * 
 * @author Bernard Che Longho (lobe1602)
 * @version 1.2
 * @since 2017-11-30
 */
package dt062g.lobe1602.assignment4;

import java.awt.Graphics;

/**
 * <code>Circle</code> This class inherits from Shape
 */
public class Circle extends Shape {
	private final double PI = 3.14;

	/** Approximate value of pi */
	/**
	 * Default constructor1 of <code>Circle</code> Takes its coordinates from x
	 * and y coordinates
	 * 
	 * @param x
	 *            The x-coordinate
	 * @param y
	 *            The y-coordinate
	 * @param col
	 *            The fill color
	 */
	public Circle(double x, double y, String col) {
		super(x, y, col);
	}

	/**
	 * Default constructor2 of <code>Circle</code> Takes its coordinates from
	 * the x- and y-coordinates from a point object
	 * 
	 * @param p
	 *            Point object with the coordinates that determine the location
	 *            of the circle
	 * @param col
	 *            The fill color of the circle
	 */
	public Circle(Point p, String col) {
		super(p, col);
	}

	/**
	 * Calculate and return the radius of a circle. If two points are given,
	 * then x2 - x1 will give us the circle
	 * 
	 * @return radius The radius of the circle
	 * @throws NoEndPointException
	 *             if no end point is provided
	 */
	public double getRadius() throws NoEndPointException {
		if (points.get(1) != null) {
			double x1 = this.points.get(0).getXcord();
			double x2 = this.points.get(1).getXcord();
			double y1 = this.points.get(0).getYcord();
			double y2 = this.points.get(1).getYcord();

			return Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
		} else {
			throw new NoEndPointException(
					"The circle's radius cannot be computed, end point is missing!");
		}
	}

	/**
	 * Calculate and return the circumference of the circle If radius is
	 * defined, use 2*pi*radius to get the circumference.
	 * 
	 * @throws NoEndPointException
	 *             if no end-point is not present
	 * @see dt062g.lobe1602.assignment4.Shape#getCircumference()
	 */
	@Override
	public double getCircumference() throws NoEndPointException {
		if (points.get(1) == null) {
			throw new NoEndPointException(
					"The circle has no end point, its circumference cannot be calculated!");
		} else {
			return 2 * PI * this.getRadius();
		}
	}

	/**
	 * Calculate and return the area of the circle. If radius is defined, use
	 * pi*radius*radius
	 * 
	 * @throws NoEndPointException
	 *             if the end point is not provided
	 * @see dt062g.lobe1602.assignment4.Shape#getArea()
	 */
	@Override
	public double getArea() throws NoEndPointException {
		if (points.get(1) == null) {
			throw new NoEndPointException(
					"The circle has no end point, its area cannot be calculated!");
		} else {
			return PI * this.getRadius() * this.getRadius();
		}
	}

	/**
	 * @see dt062g.lobe1602.assignment4.Shape#draw()
	 */
	@Override
	public void draw() {
		System.out.println(this.toString());
	}

	/**
	 * @see dt062g.lobe1602.assignment4.Shape#draw(java.awt.Graphics)
	 */
	@Override
	public void draw(Graphics g) {
	}

	/**
	 * Overload the toString method to print a circle object on the screen If
	 * there is a second point, this prints the circle's details, otherwise The
	 * exception thrown by other methods are caught but nothing is done with
	 * them. Instead, another string of interest is returned.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		try {
			return this.getClass().getSimpleName() + "[start="
					+ this.points.get(0) + "; end=" + this.points.get(1)
					+ "; radius=" + this.getRadius() + "; color= "
					+ this.getColor() + "]";
		} catch (NoEndPointException e) { // Catch and do nothing with the
										  // exception but return message of
										  // interest
			return this.getClass().getSimpleName() + "[start="
					+ this.points.get(0) + "; end=N/A; radius=N/A;" + " color= "
					+ this.getColor() + "]";
		}
	}
}
