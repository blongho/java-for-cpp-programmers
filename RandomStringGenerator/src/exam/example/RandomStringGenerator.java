/**
 * @file : RandomStringGenerator - RandomStringGenerator.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-18
 * @version : 1.0
 */
package exam.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomStringGenerator {

	/**
	 * allChars
	 */
	private static List<Character> allChars;

	/**
	 * 
	 */
	public RandomStringGenerator() {

	}

	/**
	 * Make a list comprised of digits. Use ASCII numbers and convert to char
	 * 
	 * @return Character list made of 0-9
	 */
	private static List<Character> digits()
	{
		List<Character> digits = new ArrayList<>();
		for (int i = 48; i <= 57; i++) {
			digits.add((char) i);
		}
		return digits;
	}

	/**
	 * List of lower case characters a-z
	 * 
	 * @return ArrayList of lower case characters
	 */
	private static List<Character> lowerCases()
	{
		List<Character> lowerCases = new ArrayList<>();
		for (int i = 97; i <= 122; i++) {
			lowerCases.add((char) i);
		}
		return lowerCases;
	}

	/**
	 * List of upper case letters A-Z
	 * 
	 * @return ArrayList of upper case characters
	 */
	private static List<Character> upperCases()
	{
		List<Character> uppercases = new ArrayList<>();
		for (int i = 65; i <= 90; i++) {
			uppercases.add((char) i);
		}
		return uppercases;
	}

	/**
	 * Build required list of characters to be used for string generation
	 * 
	 * @param s
	 *            the current randomStringSettings
	 * @return an ArrayList of based on settings
	 */
	private static List<Character> requiredList(RandomStringSetting s)
	{
		List<Character> chars = new ArrayList<>();

		if (s.isUseDigits()) {
			chars.addAll(digits());
		}
		if (s.isUseLowerCase()) {
			chars.addAll(lowerCases());
		}

		if (s.isUseUpperCase()) {
			chars.addAll(upperCases());
		}
		return chars;
	}

	/**
	 * Make a random string of length @see RandomStringSetting.java#setValues()
	 * 
	 * @param setting
	 *            Current settings governing string generation
	 * @return String a string that is generated
	 */
	private static String makeString(RandomStringSetting setting)
	{
		StringBuffer stringBuffer = new StringBuffer("");

		Random random = new Random();

		for (int i = 0; i < setting.getStringLength(); i++) {

			int num = random.nextInt(allChars.size());

			stringBuffer.append(allChars.get(num));
		}
		return stringBuffer.toString();

	}

	/**
	 * Generate the required number of random strings
	 * 
	 * @param setting
	 */
	public static RandomStrings generate(RandomStringSetting setting)
	{
		RandomStrings randomStrings = new RandomStrings(setting);
		allChars = requiredList(setting);

		int tries = 0;

		int numOfStrings = setting.getNumberOfStrings();
		while (randomStrings.getStrings().size() < numOfStrings) {
			String string = makeString(setting);
			tries++;
			randomStrings.addString(string);
			if (tries >= (numOfStrings * 600)) {
				System.err.print("You requested " + setting.getNumberOfStrings() + " strings but there are only ");
				System.err.print(randomStrings.getStrings().size() + " possible (based on your criteria)");
				System.err.println();
				break;
			}

		}
		return randomStrings;
	}

}
