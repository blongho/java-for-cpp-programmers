/**
 * @file : RandomStringGenerator - TestRandomStrings.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-18
 * @version : 1.0
 */
package exam.example;

import java.util.Collections;

/**
 * TestRandomStrings
 * 
 */
public class TestRandomStrings {

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		RandomStringSetting setting = new RandomStringSetting(100, 4, false, true, true, true);

		long start = System.currentTimeMillis();
		RandomStrings randomStrings = new RandomStrings(setting);
		randomStrings = RandomStringGenerator.generate(setting);

		long end = System.currentTimeMillis();
		// System.out.println(setting);
		System.out.println("Time elapsed: " + (end - start) + " milliseconds");
		System.out.println("Requests: " + setting.getNumberOfStrings());
		System.out.println("Generated: " + randomStrings.getStrings().size());
		Collections.sort(randomStrings.getStrings());
		randomStrings.displayStrings();

		// System.out.println(stringBuffer);
	}

}
