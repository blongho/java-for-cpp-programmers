/**
 * @file : RandomStringGenerator - RandomStringGUI.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-18
 * @version : 1.0
 */
package exam.example;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class RandomStringGUI extends JFrame implements ActionListener, ItemListener {

	private final int MAX_NUMBER_OF_STRINGS = RandomStringSetting.MAX_NUMBER_OF_STRINGS;
	private final int MIN_NUMBER_OF_STRINGS = RandomStringSetting.MIN_NUMBER_OF_STRINGS;
	private final int INIT_NUMBER_OF_STRINGS = 100;

	private final int MAX_STRING_LENGTH = RandomStringSetting.MAX_STRING_LENGTH;
	private final int MIN_STRING_LENGTH = RandomStringSetting.MIN_STRING_LENGTH;
	private final int INIT_STRING_LENGTH = 3;

	private JPanel settingsPanel;
	private JPanel resultsPanel;
	private JButton generateStringsButton;
	private JButton clearResultsButton;
	private JButton saveToTextButton;
	private JCheckBox useDigitCheckBox;
	private JCheckBox useUpperCaseCheckBox;
	private JCheckBox useLowerCaseCheckBox;
	private JCheckBox uniqueStringsCheckBox;
	private JTextField numberOfStringField;
	private JTextField stringLengthField;
	private JTextArea resultsArea;

	private RandomStrings randomStrings;

	public RandomStringGUI() {
		initiateInstanceVariables();
		makeFrame();
	}

	public static void main(String[] args)
	{
		RandomStringGUI g = new RandomStringGUI();
		g.setVisible(true);
	}

	private void initiateInstanceVariables()
	{

	}

	private void makeFrame()
	{
		setLayout(new GridLayout(1, 2));
		setTitle("Random string generator");
		setSize(650, 350);
		settingsPanel = getSettingsPanel();
		add(settingsPanel);
		resultsPanel = getResultsPanel();
		add(resultsPanel);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		generate();
	}

	private void exit()
	{
		System.exit(0);
	}

	private void generate()
	{
		RandomStringSetting setting = new RandomStringSetting(50, 5, true, true, false, true);
		RandomStrings randomStrings = new RandomStrings(setting);
		randomStrings = RandomStringGenerator.generate(setting);
		List<String> strings = randomStrings.getStrings();
		StringBuffer stringBuffer = new StringBuffer("");
		int i = 0;
		for (String string : strings) {
			i++;
			stringBuffer.append(string + "    ");
			if (i % 5 == 0) {
				stringBuffer.append("\n");
			}
		}
		System.out.println(stringBuffer);
		resultsArea.setText(stringBuffer.toString());
	}

	private void clear()
	{
		resultsArea.setText("");
		randomStrings = null;
	}

	private void saveToTextFile()
	{

	}

	private File getFile(String type)
	{
		File file = new File(".");
		JFileChooser chooser = new JFileChooser();
		return file;
	}

	private void setResults(ArrayList<String> strings)
	{
		resultsArea.setText("");

		for (String s : strings) {
			resultsArea.append(s + "  ");
		}
	}

	private JCheckBox checkbox(String title, boolean selected)
	{
		JCheckBox checkbox = new JCheckBox(title, selected);
		checkbox.addItemListener(this);
		return checkbox;
	}

	private JPanel getSettingsPanel()
	{
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(11, 1));
		panel.add(new JLabel("Number of strings"));
		numberOfStringField = new JTextField(Integer.toString(INIT_NUMBER_OF_STRINGS), 5);

		numberOfStringField.addActionListener(this);
		numberOfStringField.setHorizontalAlignment(SwingConstants.LEFT);
		panel.add(numberOfStringField);

		panel.add(new JLabel("String length"));
		stringLengthField = new JTextField(Integer.toString(INIT_STRING_LENGTH), 10);
		stringLengthField.setHorizontalAlignment(SwingConstants.LEFT);
		stringLengthField.addActionListener(this);

		panel.add(stringLengthField);

		useDigitCheckBox = checkbox("Use digits (0-9)", true);
		useLowerCaseCheckBox = checkbox("Use lower case (a-z)", false);
		useUpperCaseCheckBox = checkbox("Use upper case (A-Z)", true);
		uniqueStringsCheckBox = checkbox("Unique strings", true);

		panel.add(useDigitCheckBox);
		panel.add(useUpperCaseCheckBox);
		panel.add(useLowerCaseCheckBox);
		panel.add(uniqueStringsCheckBox);

		// panel.setBorder(BorderFactory.createEmptyBorder(10, 2, 5, 10));
		panel.setPreferredSize(new Dimension(80, 350));
		panel.setBorder(BorderFactory.createTitledBorder("Settings"));
		return panel;
	}

	/**
	 * @return the resultsPanel
	 */
	public JPanel getResultsPanel()
	{
		resultsPanel = new JPanel();
		resultsPanel.setBorder(BorderFactory.createTitledBorder("Results"));
		resultsArea = new JTextArea(100, 300);
		resultsArea.setVisible(true);
		resultsPanel.add(new JScrollPane(resultsArea));
		return resultsPanel;
	}

	private void updateSettings(RandomStringSetting settings)
	{

	}

	/**
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == generateStringsButton) {
			generate();
		} else if (e.getSource() == clearResultsButton) {
			clear();
		} else if (e.getSource() == saveToTextButton) {
			// saveToText();
		}

	}

	/**
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	@Override
	public void itemStateChanged(ItemEvent arg0)
	{
		// TODO Auto-generated method stub

	}

}
