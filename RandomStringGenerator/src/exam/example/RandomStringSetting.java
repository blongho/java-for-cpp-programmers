/**
 * @file : RandomStringGenerator - RandomStringSetting.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-18
 * @version : 1.0
 */
package exam.example;

/**
 * RandomStringSetting <br>
 * Class that defines the settings of the randomString generator project
 */
public class RandomStringSetting {
	static int MIN_STRING_LENGTH = 1;
	static int MAX_STRING_LENGTH = 20;
	static int MIN_NUMBER_OF_STRINGS = 1;
	static int MAX_NUMBER_OF_STRINGS = 100000;
	private int numberOfStrings = MIN_NUMBER_OF_STRINGS; // number of strings to generate
	private int stringLength = MIN_STRING_LENGTH; // length of the string to generates
	private boolean useDigits; // use 0-9 or not
	private boolean useUpperCase; // use A-Z or not
	private boolean useLowerCase; // use a-z or not
	private boolean uniqueStrings; // allow for repeated string or not

	/**
	 * Constructor with all parameters
	 * 
	 * @param nos
	 *            number of strings
	 * @param lenght
	 *            length of string
	 * @param digits
	 *            true digits will be used
	 * @param uCase
	 *            true if upperCase numbers will be used
	 * @param lCase
	 *            true if lower case numbers will be used
	 * @param unique
	 */
	public RandomStringSetting(int nos, int lenght, boolean digits, boolean uCase, boolean lCase, boolean unique) {
		numberOfStrings = nos;
		stringLength = lenght;
		useUpperCase = uCase;
		useLowerCase = lCase;
		if (!uCase && !lCase) {
			digits = true;
		}
		useDigits = digits;
		uniqueStrings = unique;
	}

	/**
	 * for testing purpose
	 */
	public RandomStringSetting() {
		if (!isUseLowerCase() && !isUseUpperCase()) {
			useDigits = true;
		}
	}

	/**
	 * @return the numberOfStrings
	 */
	public int getNumberOfStrings()
	{
		return numberOfStrings;
	}

	/**
	 * @param numberOfStrings
	 *            the numberOfStrings to set
	 */
	public void setNumberOfStrings(int num)
	{
		if (num < MIN_NUMBER_OF_STRINGS) {
			numberOfStrings = MIN_NUMBER_OF_STRINGS;
			System.err.println(num + " is <the min number. Number of strings set to " + MIN_NUMBER_OF_STRINGS);
		} else if (num > MAX_NUMBER_OF_STRINGS) {
			numberOfStrings = MAX_NUMBER_OF_STRINGS;
			System.err.println(num + " is > the max number. Number of strings set to " + MAX_NUMBER_OF_STRINGS);
		} else {
			numberOfStrings = num;
		}
	}

	/**
	 * @return the stringLength
	 */
	public int getStringLength()
	{
		return stringLength;
	}

	/**
	 * @param length
	 *            the stringLength to set
	 */
	public void setStringLength(int length)
	{
		if (length < MIN_STRING_LENGTH) {
			stringLength = MIN_STRING_LENGTH;
			System.err.println(length + " is < the min number. Number of strings set to " + MIN_STRING_LENGTH);
		} else if (length > MAX_STRING_LENGTH) {
			stringLength = MAX_STRING_LENGTH;
			System.err.println(length + " is > the max string lenghth. String length set to " + MAX_STRING_LENGTH);
		} else {
			stringLength = length;
		}
	}

	/**
	 * @return the useDigits
	 */
	public boolean isUseDigits()
	{
		return useDigits;
	}

	/**
	 * @param useDigits
	 *            the useDigits to set
	 */
	public void setUseDigits(boolean digits)
	{
		if (!isUseLowerCase() && !isUseUpperCase()) {
			useDigits = true;
		} else {
			useDigits = digits;
		}
	}

	/**
	 * @return the useUpperCase
	 */
	public boolean isUseUpperCase()
	{
		return useUpperCase;
	}

	/**
	 * @param useUpperCase
	 *            the useUpperCase to set
	 */
	public void setUseUpperCase(boolean useUpperCase)
	{
		this.useUpperCase = useUpperCase;
	}

	/**
	 * @return the useLowerCase
	 */
	public boolean isUseLowerCase()
	{
		return useLowerCase;
	}

	/**
	 * @param useLowerCase
	 *            the useLowerCase to set
	 */
	public void setUseLowerCase(boolean useLowerCase)
	{
		this.useLowerCase = useLowerCase;
	}

	/**
	 * @return the uniqueStrings
	 */
	public boolean isUniqueStrings()
	{
		return uniqueStrings;
	}

	/**
	 * @param uniqueStrings
	 *            the uniqueStrings to set
	 */
	public void setUniqueStrings(boolean uniqueStrings)
	{
		this.uniqueStrings = uniqueStrings;
	}

	/**
	 * setValues <br>
	 * Sets all the values of this object. This will be used in the GUI
	 * 
	 * @see {@link #RandomStringSetting(int, int, boolean, boolean, boolean, boolean)}
	 */
	public void setValues(int nos, int lenght, boolean digits, boolean uCase, boolean lCase, boolean unique)
	{
		setNumberOfStrings(nos);
		setStringLength(lenght);
		setUseDigits(digits);
		setUseUpperCase(uCase);
		setUseLowerCase(lCase);
		setUniqueStrings(unique);
	}

	/**
	 * String representation of the settings
	 * 
	 * @see java.lang.Object#toString()
	 */
	@SuppressWarnings("boxing")
	@Override
	public String toString()
	{
		return String.format(
						"Settings %nNumber of strings %-15d%nString length %5d%nUse digits %11b%n"
										+ "Use upperCase %8b%nUse lowerCase %8b%nUnique strings%8b",
						numberOfStrings, stringLength, useDigits, useUpperCase, useLowerCase, uniqueStrings);
	}
}
