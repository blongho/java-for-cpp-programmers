/**
 * @file : RandomStringGenerator - RandomStrings.java
 * @author : Bernard Che Longho (lobe1602)
 * @since : 2018-01-18
 * @version : 1.0
 */
package exam.example;

import java.util.ArrayList;
import java.util.List;

/**
 * RandomStrings <br>
 * Class to save the random strings
 * 
 */
public class RandomStrings {

	private RandomStringSetting setting;
	private List<String> Strings;

	/**
	 * Constructor
	 * 
	 * @param setting
	 *            The setting to be used to generate the random strings.
	 */
	public RandomStrings(RandomStringSetting setting) {
		this.setting = setting;
		Strings = new ArrayList<String>(setting.getNumberOfStrings());
	}

	/**
	 * for testing
	 */
	public RandomStrings() {
		setting = new RandomStringSetting();
	}

	/**
	 * @return the setting required for the random string generator
	 */
	public RandomStringSetting getSetting()
	{
		return setting;
	}

	/**
	 * Make changes to the settings
	 * 
	 * @param setting
	 */
	public void setSetting(RandomStringSetting setting)
	{
		this.setting = setting;
	}

	/**
	 * Add a string in the containers
	 * 
	 * @param string
	 * @return
	 */
	public boolean addString(String string)
	{
		if (Strings.isEmpty()) { // if empty, add and go away
			Strings.add(string);
			return true;
		}
		if (!Strings.isEmpty()) { // there are values
			if (setting.isUniqueStrings()) { // unique is specified
				if (Strings.indexOf(string) < 0) {
					Strings.add(string);
					return true;
				} else {
					return false;
				}
			} else { // !isUseUniqueStrings()
				Strings.add(string);
				return true;
			}
		}
		return false;
	}

	/**
	 * Get the strings in the Strings array
	 * 
	 * @return Strings
	 */
	public List<String> getStrings()
	{
		return Strings;
	}

	/**
	 * Display info contained in the Strings
	 */
	public void displayStrings()
	{
		StringBuffer display = new StringBuffer("");
		int idx = 0;
		String space = "    ";
		if (!Strings.isEmpty()) {
			display.append("Generated strings...\n");
			for (String string : Strings) {
				display.append(string + space);
				idx++;
				if (idx % 5 == 0) {
					display.append("\n");
				}

			}
		}
		System.out.println(display);
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return this.getClass().getSimpleName() + " is working with the following\n" + setting.toString();
	}

}
